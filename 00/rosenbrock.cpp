// ************************************************************************** //
//       Introduction to Algorithmic Differentiation SS23 - Tutorial 0        //
//                                                                            //
//                      Introduction & Environment setup                      //
//                                                                            //
// Copyright (C) 2023 Software and Tools for Computational Engineering (STCE) //
//               RWTH Aachen University - www.stce.rwth-aachen.de             //
// ************************************************************************** //

// Include C++ standard header
#include <array>
#include <cmath>
#include <iostream>

// Include our Eigen wrappers and dco/c++
#include <Eigen.hpp>
#include <dco.hpp>

static constexpr size_t N = 2;

// ****************************** Custom Types ****************************** //

// Tangent type
template <typename T> using tangent_t = typename dco::gt1s<T>::type;

// Adjoint mode
template <typename T> using adjoint_m = dco::ga1s<T>;

// Adjoint type
template <typename T> using adjoint_t = typename adjoint_m<T>::type;

// Nx1 Vector
template <typename T> using vec = Eigen::row_vector_t<T, N>;

// NxN Matrix
template <typename T> using mat = Eigen::matrix_t<T, N, N>;

// ***************************** Primal function **************************** //

// Rosenbrock function
template <typename T> T f(vec<T> x) {
  return pow(1 - x(0), 2) + 100 * pow(-pow(x(0), 2) + x(1), 2);
}

// ******************************* Gradients ******************************** //

// Analytical gradient of Rosenbrock function
template <typename T> vec<T> grad(vec<T> x) {
  vec<T> g;
  g(0) = -400 * x(0) * (-pow(x(0), 2) + x(1)) + 2 * x(0) - 2;
  g[1] = -200 * pow(x(0), 2) + 200 * x(1);
  return g;
}

// Central finite differences gradient of Rosenbrock function
template <typename T> vec<T> grad_fd(vec<T> x) {
  vec<T> g;
  T h = 1e-5;
  vec<T> x_m, x_p;

  x_m = x;
  x_p = x;
  for (size_t i = 0; i < x.size(); i++) {
    x_m(i) -= h;
    x_p(i) += h;

    g(i) = (f(x_p) - f(x_m)) / (2 * h);

    x_m(i) = x(i);
    x_p(i) = x(i);
  }
  return g;
}

// Tangent mode gradient of Rosenbrock function
template <typename T> vec<T> grad_tangent(vec<T> x) {
  vec<T> g;
  vec<tangent_t<T>> xt = {x(0), x(1)};

  for (size_t i = 0; i < x.size(); i++) {
    dco::derivative(xt(i)) = 1.0;
    g(i) = dco::derivative(f(xt));
    dco::derivative(xt(i)) = 0.0;
  }
  return g;
}

// Adjoint mode gradient of Rosenbrock function
template <typename T> vec<T> grad_adjoint(vec<T> x) {
  vec<T> g;
  vec<adjoint_t<T>> xa = {x(0), x(1)};

  dco::smart_tape_ptr_t<adjoint_m<T>> tape;
  for (size_t i = 0; i < x.size(); i++) {
    tape->register_variable(xa(i));
  }

  adjoint_t<T> ya = f(xa);
  dco::derivative(ya) = 1.0;
  tape->interpret_adjoint();

  for (size_t i = 0; i < x.size(); i++) {
    g(i) = dco::derivative(xa(i));
  }

  return g;
}

// ******************************** Hessians ******************************** //

// Analytical hessian of Rosenbrock function
template <typename T> mat<T> hessian(vec<T> x) {
  mat<T> H;
  H(0, 0) = 2 * (600 * pow(x(0), 2) - 200 * x(1) + 1);
  H(0, 1) = -400 * x(0);
  H(1, 0) = -400 * x(0);
  H(1, 1) = 200;
  return H;
}

// Tangent over Tangent mode hessian of Rosenbrock function
template <typename T> mat<T> hessian_tot(vec<T> x) {
  using TA_tot = tangent_t<tangent_t<T>>;
  vec<TA_tot> x_tot = {x(0), x(1)};
  mat<T> H;

  for (size_t i = 0; i < N; i += 1) {
    dco::value(dco::derivative(x_tot(i))) = 1.0; // seed x^{(1)}

    for (size_t j = 0; j < N; j += 1) {
      dco::derivative(dco::value(x_tot(j))) = 1.0; // seed x^{(2)}

      TA_tot y_tot = f(x_tot);
      H(i, j) =
          dco::derivative(dco::derivative(y_tot)); // harvest second derivative

      dco::derivative(dco::value(x_tot(j))) = 0.0; // reset x^{(1)}
    }

    dco::value(dco::derivative(x_tot(i))) = 0.0; // reset x^{(2)}
  }

  return H;
}

// Tangent over Adjoint mode hessian of Rosenbrock function
template <typename T> mat<T> hessian_toa(vec<T> x) {
  using TA_toa = adjoint_t<tangent_t<T>>;
  vec<TA_toa> x_toa = {x(0), x(1)};
  mat<T> H;

  dco::smart_tape_ptr_t<adjoint_m<tangent_t<T>>> tape;

  for (size_t i = 0; i < N; i += 1) {
    tape->register_variable(x_toa(i));
  }

  auto tpos = tape->get_position(); // store tape position

  for (size_t i = 0; i < N; i += 1) {
    dco::derivative(dco::value(x_toa(i))) = 1.0; // seed tangent x^{(1)}

    TA_toa y_toa = f(x_toa);

    dco::value(dco::derivative(y_toa)) = 1.0; // seed adjoint y_{(2)}
    tape->interpret_adjoint();

    for (size_t j = 0; j < N; j += 1) {
      H(i, j) = dco::derivative(
          dco::derivative(x_toa(j))); // harvest 2nd derivative x^{(1)}_{(2)}
      dco::value(dco::derivative(x_toa(j))) = 0.0; // reset adjoint x_{(2)}
      dco::derivative(dco::derivative(x_toa(j))) =
          0.0; // reset 2nd derivative x^{(1)}_{(2)}
    }

    tape->reset_to(tpos); // Reset tape so that the inputs are still registered

    dco::derivative(dco::value(x_toa(i))) = 0.0; // reset tangent x^{(1)}
  }

  return H;
}

// Adjoint over Tangent mode hessian of Rosenbrock function
template <typename T> mat<T> hessian_aot(vec<T> x) {
  using TA_aot = tangent_t<adjoint_t<T>>;
  vec<TA_aot> x_aot = {x(0), x(1)};
  mat<T> H;

  dco::smart_tape_ptr_t<adjoint_m<T>> tape;

  for (size_t i = 0; i < N; i += 1) {
    tape->register_variable(dco::value(x_aot(i)));
    tape->register_variable(dco::derivative(x_aot(i)));
  }

  auto tpos = tape->get_position(); // store tape position

  for (size_t i = 0; i < N; i += 1) {
    dco::value(dco::derivative(x_aot(i))) = 1.0; // seed x^{(1)}

    TA_aot y_aot = f(x_aot);
    dco::derivative(dco::derivative(y_aot)) = 1.0; // seed x^{(1)}_{(2)}
    tape->interpret_adjoint();

    for (size_t j = 0; j < N; j += 1) {
      H(i, j) =
          dco::derivative(dco::value(x_aot(j))); // harvest second derivative
      dco::derivative(dco::value(x_aot(j))) =
          0.0; // reset 2nd derivative x^{(1)}_{(2)}
      dco::derivative(dco::value(x_aot(j))) = 0.0; // reset x^{(1)}
    }

    tape->reset_to(tpos); // Reset tape so that the inputs are still registered

    dco::value(dco::derivative(x_aot(i))) = 0.0; // reset x^{(1)}
  }

  return H;
}

// Adjoint over Adjoint mode hessian of Rosenbrock function
template <typename T> mat<T> hessian_aoa(vec<T> x) {
  using TA_aoa = adjoint_t<adjoint_t<T>>;
  vec<TA_aoa> x_aoa = {x(0), x(1)};
  mat<T> H;

  dco::smart_tape_ptr_t<adjoint_m<T>> tape_1;
  dco::smart_tape_ptr_t<adjoint_m<adjoint_t<T>>> tape_2;

  for (size_t i = 0; i < N; i += 1) {
    tape_1->register_variable(dco::value(x_aoa(i)));
    tape_2->register_variable(x_aoa(i));
  }

  TA_aoa y_aoa = f(x_aoa);

  dco::value(dco::derivative(y_aoa)) = 1.0; // seed x_{(1)}
  tape_2->interpret_adjoint();

  for (size_t i = 0; i < N; i += 1) {
    dco::derivative(dco::derivative(x_aoa(i))) = 1.0; // seed x_{(1,2)}
    tape_1->interpret_adjoint();

    for (size_t j = 0; j < N; j += 1) {
      H(i, j) =
          dco::derivative(dco::value(x_aoa(j))); // harvest second derivative
    }

    tape_1->zero_adjoints();
  }

  return H;
}

// ********************************** Main ********************************** //

int main() {
  using PT = double;
  vec<PT> x = {-0.3, 2.0};

  std::cout << "x = " << x << std::endl;
  std::cout << "f = " << f(x) << std::endl;
  std::cout << "gradient: " << grad(x) << std::endl;
  std::cout << "gradient (finite differences): " << grad_fd(x) << std::endl;
  std::cout << "gradient (tangent): " << grad_tangent(x) << std::endl;
  std::cout << "gradient (adjoint): " << grad_adjoint(x) << std::endl;
  std::cout << "hessian: \n" << hessian(x) << std::endl;
  std::cout << "hessian (tangent over tangent): \n"
            << hessian_tot(x) << std::endl;
  std::cout << "hessian (tangent over adjoint): \n"
            << hessian_toa(x) << std::endl;
  std::cout << "hessian (adjoint over tangent): \n"
            << hessian_aot(x) << std::endl;
  std::cout << "hessian (adjoint over adjoint): \n"
            << hessian_aoa(x) << std::endl;

  return 0;
}

# Some answers to HW3

### Surface plot
To create a surface point cloud call `log_rosenbrock_surface` in the main loop.

### FD and analytic comparison
The functions compare quite well, except for the value of
`ddRddx0`. The delta between this value produced by FD computed
hessian and analytically computed hessian is of the order of 10e-3.

### Minimum of Rosenbrock
Since the function is quadratic in both terms containing x0 and x1,
I would expect a minimum to be along the curves of:
```
(a - x0)^2 = 0 && (x1 - x0^2)^2 = 0 
```
Which would probably be x0 = a, x1 = a^2

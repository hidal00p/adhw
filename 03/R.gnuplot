#!/usr/bin/env gnuplot

set key at screen 1, 0.9 right top vertical Right noreverse enhanced autotitle nobox
unset key
set style textbox  opaque margins  0.5,  0.5 fc  bgnd noborder linewidth  1.0
unset parametric
set view 20, 340, 1, 1.1
set isosamples 50, 50
#set cntrlabel  format '%8.3g' font ',7' start 2 interval 20
set hidden3d back offset 1 trianglepattern 3 undefined 1 altdiagonal bentover
set cntrparam order 8
#set style data polygons
set xyplane relative 0
set ztics  norangelimit logscale autofreq
set title "Rosenbrock Function"
set xlabel "x"
set xrange [ * : * ] noreverse writeback
set x2range [ * : * ] noreverse writeback
set ylabel "y"
set yrange [ * : * ] noreverse writeback
set y2range [ * : * ] noreverse writeback
set zlabel "Z "
set zlabel offset character 1, 0, 0 font "" textcolor lt -1 norotate
set zrange [ * : * ] noreverse writeback
set cbrange [ * : * ] noreverse writeback
set rrange [ * : * ] noreverse writeback
set logscale z 10
set colorbox vertical origin screen 0.9, 0.2 size screen 0.05, 0.6 front  noinvert bdefault
Rosenbrock(x,y) = (1-x)**2 + 100*(y - x**2)**2
NO_ANIMATION = 1

set contour base

splot [-1.5:1.5] [-0.5:1.5] Rosenbrock(x,y)

pause mouse close

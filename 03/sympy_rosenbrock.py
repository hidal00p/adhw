#! /usr/bin/env python3

from sympy import init_printing, symbols, IndexedBase, Idx, ccode, diff
from sympy import Function, Matrix, pprint

init_printing()

# Init symbolic vector and calculate Rosenbrock function
x = IndexedBase("x", shape=(2,))
R = (1 - x[0]) ** 2 + 100 * (x[1] - x[0] ** 2) ** 2

print("R:")
pprint(R)
print()
print("Primal code:")
print(ccode(R) + ";")
print()

// ************************************************************************** //
//       Introduction to Algorithmic Differentiation SS23 - Tutorial 3        //
//                                                                            //
//       Multivariate Finite Differences & 2nd Order Finite Differences       //
//                                                                            //
// Copyright (C) 2023 Software and Tools for Computational Engineering (STCE) //
//               RWTH Aachen University - www.stce.rwth-aachen.de             //
// ************************************************************************** //

// Include Eigen
#include <Eigen/Dense>

// Include C++ standard header
#include <algorithm>
#include <array>
#include <cassert>
#include <cmath>
#include <iostream>

using namespace std;

// ****************************** Custom types ****************************** //

//! Vector with 2 entries (inputs & Gradient)
template <typename T> using vec2_t = Eigen::Vector2<T>;

//! Matrix with 2x2 entries (Hessian)
template <typename T> using mat2_t = Eigen::Matrix2<T>;

// ***************************** Primal function **************************** //

/******************************************************************************
 * @brief Evaluate the Rosenbrock function.
 *
 * @param[in] x Input vector x.
 * @returns Result of primal function.
 ******************************************************************************/
template <typename T> T R(const vec2_t<T> &x) {
  T a = 1.0;
  T b = 100.0;
  T x0 = x[0];
  T x1 = x[1];

  T term1 = pow(a - x0, 2);
  T term2 = b * pow(x1 - x0 * x0, 2);

  return term1 + term2;
}

// ************************* Analytical Derivatives ************************* //

/******************************************************************************
 * @brief Calculate the analytical Gradient of the Rosenbrock function.
 *
 * @param[in] x Input vector x.
 * @returns The analytical Gradient of the Rosenbrock function.
 ******************************************************************************/
template <typename T> vec2_t<T> dRdx(const vec2_t<T> &x) {
  T a = 1.0;
  T b = 100.0;
  T x0 = x[0];
  T x1 = x[1];

  T dRdx1 = 2 * b * (x1 - x0 * x0);
  T dRdx0 = -2 * (a - x0 + dRdx1 * x0);

  return vec2_t<T>({dRdx0, dRdx1});
}

/******************************************************************************
 * @brief Calculate the analytical Hessian of the Rosenbrock function.
 *
 * @param[in] x Input vector x.
 * @returns The analytical Hessian of the Rosenbrock function.
 ******************************************************************************/
template <typename T> mat2_t<T> d2Rdx2(const vec2_t<T> &x) {
  T a = 1.0;
  T b = 100.0;
  T x0 = x[0];
  T x1 = x[1];

  T dRdx1 = 2 * b * (x1 - x0 * x0);
  T ddRdx1dx0 = -4 * b * x0;
  T ddRddx0 = 2 * (1 - dRdx1 - ddRdx1dx0 * x0);
  T ddRddx1 = 2 * b;

  return mat2_t<T>({{ddRddx0, ddRdx1dx0}, {ddRdx1dx0, ddRddx1}});
}

// *************************** Finite Differences *************************** //

/******************************************************************************
 * @brief Calculate the finite difference step size depending on the base type
 *        and order of the derivative.
 *
 * @param[in] order Derivative order.
 * @param[in] x Input vector x.
 * @param[in] alternative_h Whether to use the alternative step size.
 * @returns Result of primal function.
 ******************************************************************************/
template <typename T>
T step_size(const size_t order, const T x, const bool alternative_h = false) {
  T eps = numeric_limits<T>::epsilon();
  for (size_t i = 0; i < order; i++) {
    eps = cbrt(eps);
  }

  T h = (x == 0) ? eps : eps * fabs(x);

  // Alternative step size
  if (alternative_h) {
    h = eps * (T(1.0) + fabs(x));
    h = pow(T(2.0), round(log(h) / log(T(2.0))));
  }

  return h;
}

/******************************************************************************
 * @brief Calculate the central finite differences Gradient of the
 *        Rosenbrock function.
 *
 * @param[in] x Input vector x.
 * @param[in] alternative_h Whether to use the alternative step size.
 * @returns The central finite differences of the smoothed primal function.
 ******************************************************************************/
template <typename T>
vec2_t<T> dRdx_fd(const vec2_t<T> &x, const bool alternative_h = false) {
  T x0 = x[0];
  T x1 = x[1];
  T dx0 = step_size(1, x0, alternative_h);
  T dx1 = step_size(1, x1, alternative_h);

  vec2_t<T> dx_inc = {x0 + dx0, x1};
  vec2_t<T> dx_dec = {x0 - dx0, x1};
  T dRdx0 = (R((dx_inc)) - R(dx_dec)) / (2 * dx0);

  dx_inc = {x0, x1 + dx1};
  dx_dec = {x0, x1 - dx1};
  T dRdx1 = (R(dx_inc) - R(dx_dec)) / (2 * dx1);

  return vec2_t<T>({dRdx0, dRdx1});
}

/******************************************************************************
 * @brief Calculate the central finite differences Hessian of the
 *        Rosenbrock function.
 *
 * @param[in] x Input vector x.
 * @param[in] alternative_h Whether to use the alternative step size.
 * @returns The central finite differences of the smoothed primal function.
 ******************************************************************************/
template <typename T>
mat2_t<T> d2Rdx2_fd(const vec2_t<T> &x, const bool alternative_h = false) {
  T x0 = x[0];
  T x1 = x[1];
  T ddx0 = step_size(2, x0, alternative_h);
  T ddx1 = step_size(2, x1, alternative_h);

  // Is this a hacky solution?
  vec2_t<T> dx_inc = {x0 + ddx0, x1};
  vec2_t<T> dx_dec = {x0 - ddx0, x1};
  vec2_t<T> ddRddx0 =
      (dRdx_fd(dx_inc, alternative_h) - dRdx_fd(dx_dec, alternative_h)) /
      (2 * ddx0); // ~ ddfddx, ddfdydx

  dx_inc = {x0, x1 + ddx1};
  dx_dec = {x0, x1 - ddx1};
  vec2_t<T> ddRddx1 =
      (dRdx_fd(dx_inc, alternative_h) - dRdx_fd(dx_dec, alternative_h)) /
      (2 * ddx1); // ~ ddfdxdy, ddfdydy

  return mat2_t<T>({{ddRddx0[0], ddRddx0[1]}, {ddRddx1[0], ddRddx1[1]}});
}

// ****************************** Optimization ****************************** //

/******************************************************************************
 * @brief Steepest descent algorithm to find the optimum of the Rosenbrock
 *        function
 *
 * @param[in] x0 Initial guess.
 * @param[in] accuracy Accuracy threshold. Algorithm stops if the norm of
 *                     the gradient falls below this accuracy.
 * @param[in] alpha Step size.
 * @param[in] verbose Whether to print verbose output or not.
 * @returns The optimmum (minimum) of the Rosenbrock function, if successful.
 ******************************************************************************/
template <typename T>
vec2_t<T> steepest_descent(const vec2_t<T> &x0, const T accuracy, const T alpha,
                           const bool verbose = true,
                           const bool is_fd = false) {

  // Init variables
  size_t k = 0;
  vec2_t<T> x = x0;
  vec2_t<T> g = is_fd ? dRdx_fd(x) : dRdx(x);

  auto print_step = [&]() {
    if (verbose) {
      std::cout << "Step " << k++ << ": x = " << x.transpose()
                << ", f(x) = " << R(x) << ", Gradient norm = " << g.norm()
                << "\n";
    } else {
      std::cout << k++ << " " << x.transpose() << " " << R(x) << " " << g.norm()
                << "\n";
    }
  };

  print_step();

  do {
    g = is_fd ? dRdx_fd(x) : dRdx(x);
    x -= alpha * g;

    print_step();

    // Check for divergence
    if (g.norm() > 1e30) {
      std::cout << "Error: Steepest Descent algorithm diverged!\n";
      break;
    }
  } while (g.norm() > accuracy);

  return x;
}

/******************************************************************************
 * @brief Newton's algorithm to find the optimum of the Rosenbrock function.
 *
 * @param[in] x0 Initial guess.
 * @param[in] accuracy Accuracy threshold. Algorithm stops if the norm of
 *                     the gradient falls below this accuracy.
 * @param[in] verbose Whether to print verbose output or not.
 * @returns The optimmum (minimum) of the Rosenbrock function, if successful.
 ******************************************************************************/
template <typename T>
vec2_t<T> newton(const vec2_t<T> &x0, const T accuracy,
                 const bool verbose = true, const bool is_fd = false) {

  // Init variables
  size_t k = 0;
  vec2_t<T> x = x0;
  vec2_t<T> g = is_fd ? dRdx_fd(x) : dRdx(x);

  auto print_step = [&]() {
    if (verbose) {
      std::cout << "Step " << k++ << ": x = " << x.transpose()
                << ", f(x) = " << R(x) << ", Gradient norm = " << g.norm()
                << "\n";
    } else {
      std::cout << k++ << " " << x.transpose() << " " << R(x) << " " << g.norm()
                << "\n";
    }
  };

  print_step();

  do {
    g = is_fd ? dRdx_fd(x) : dRdx(x);
    mat2_t<T> H = is_fd ? d2Rdx2_fd(x) : d2Rdx2(x);

    // Perform Newton step
    x -= H.lu().solve(g);

    print_step();

    // Check for divergence
    if (g.norm() > 1e30) {
      std::cout << "Error: Newton algorithm diverged!\n";
      break;
    }
  } while (g.norm() > accuracy);

  return x;
}

template <typename T> void log_rosenbrock_surface() {
  T a_boundary = -1.0;
  T b_boundary = 1.0;
  int num_nodes = 400;

  T step = (b_boundary - a_boundary) / num_nodes;
  for (int i = 0; i < num_nodes; ++i) {
    for (int j = 0; j < num_nodes; ++j) {
      T x0 = a_boundary + i * step;
      T x1 = a_boundary + j * step;
      vec2_t<T> x = {x0, x1};
      T y = R(x);
      std::cout << x0;
      std::cout << " " << x1;
      std::cout << " " << y;
      std::cout << std::endl;
    }
  }
}

template <typename T>
void test_gradient(vec2_t<T> test_vector, bool alternative_h = false) {
  vec2_t<T> _dRdx = dRdx(test_vector);
  vec2_t<T> _dRdx_fd = dRdx_fd(test_vector, alternative_h);

  std::cout << _dRdx - _dRdx_fd << std::endl;
}

template <typename T>
void test_hessian(vec2_t<T> test_vector, bool alternative_h = false) {
  mat2_t<T> _d2Rdx2 = d2Rdx2(test_vector);
  mat2_t<T> _d2Rdx2_fd = d2Rdx2_fd(test_vector, alternative_h);

  std::cout << _d2Rdx2 - _d2Rdx2_fd << std::endl;
}

// ********************************** Main ********************************** //
int main(const int argc, const char *argv[]) {
  using T = double;

  assert(argc <= 5);

  // Accuracy
  T accuracy = 1e-6;
  if (argc > 1) {
    accuracy = static_cast<T>(stod(argv[1]));
  }

  // Alpha
  T alpha = 1e-3;
  if (argc > 2) {
    alpha = static_cast<T>(stod(argv[2]));
  }

  // Verbosity
  bool verbose = true;
  if (argc > 3) {
    verbose = static_cast<bool>(stoi(argv[3]));
  }

  bool is_fd = false;
  if (argc > 3) {
    is_fd = static_cast<bool>(stoi(argv[4]));
  }

  vec2_t<T> x0 = {-0.5, 2};
  vec2_t<T> x_min_1 = steepest_descent(x0, accuracy, alpha, verbose, is_fd);
  std::cout << "\n\n";
  vec2_t<T> x_min_2 = newton(x0, accuracy, verbose, is_fd);

  return 0;
}

// ************************************************************************** //
//       Introduction to Algorithmic Differentiation SS23 - Tutorial 2        //
//                                                                            //
//                         Univariate Scalar Tangents                         //
//                                                                            //
// Copyright (C) 2023 Software and Tools for Computational Engineering (STCE) //
//               RWTH Aachen University - www.stce.rwth-aachen.de             //
// ************************************************************************** //

// Include C++ standard header
#include <algorithm>
#include <array>
#include <cassert>
#include <cmath>
#include <dco.hpp>
#include <iostream>

using namespace std;

// ***************************** Primal function **************************** //

/******************************************************************************
 * @brief Calculate the (non-differentiable) primal function.
 *
 * @param[in] x Input value x.
 * @returns Result of primal function.
 ******************************************************************************/
template <typename T> T f(T x) {
  return max(T(1.0), T(80) * sin(x) + pow(x, T(3.0)) + T(1.0));
}

// ******************************* Smoothing ******************************** //

/******************************************************************************
 * @brief Calculate sigmoid function dependent on input value, position of the
 *        non-differentiability and the smoothing factor.
 *
 * @param[in] x Input value x.
 * @param[in] p Position of the non-differentiability.
 * @param[in] w Smoothing factor.
 * @returns Result of sigmoid function.
 ******************************************************************************/
template <typename T, typename PT> T s(T x, const PT p, const PT w) {
  return T(1.0) / (T(1.0) + exp(-(x - p) / w));
}

/******************************************************************************
 * @brief Calculate smoothed function dependent on input value and the
 *        smoothing factor.
 *
 * @param[in] x Input value x.
 * @param[in] w Smoothing factor.
 * @returns Result of smoothed primal function.
 ******************************************************************************/
template <typename T, typename PT> T fs(const T x, const PT w) {
  PT p = 0.0; // Primal function is non-differentiable at x=0

  T a = 1.0;
  T b = T(80.0) * sin(x) + pow(x, T(3.0)) + 1;
  T c = s(x, p, w);

  return a * (T(1.0) - c) + b * c;
}

// ************************** Analytical Gradients ************************** //

/******************************************************************************
 * @brief Calculate the analytical gradient of the primal function.
 *
 * @param[in] x Input value x.
 * @returns The analytical gradient of the primal function.
 ******************************************************************************/
template <typename T> T dfdx(T x) {
  if (x > T(0.0)) {
    return T(80.0) * cos(x) + T(3.0) * pow(x, T(2.0));
  }

  return T(0.0);
}

/******************************************************************************
 * @brief Calculate the analytical gradient of the sigmoid function.
 *
 * @param[in] x Input value x.
 * @param[in] p Position of the non-differentiability.
 * @param[in] w Smoothing factor.
 * @returns The analytical gradient of the sigmoid function.
 ******************************************************************************/
template <typename T, typename PT> T dsdx(T x, const PT p, const PT w) {
  T tmp = exp(-(x - p) / w);
  return tmp / (w * pow(T(1.0) + tmp, T(2.0)));
}

/******************************************************************************
 * @brief Calculate the analytical gradient of the smoothed function.
 *
 * @param[in] x Input value x.
 * @param[in] w Smoothing factor.
 * @returns The analytical gradient of the smoothed primal function.
 ******************************************************************************/
template <typename T, typename PT> T dfsdx(T x, const PT w) {
  PT p = 0.0;

  T a = 1;
  T dadx = 0;

  T b = T(80.0) * sin(x) + pow(x, T(3.0)) + 1;
  T dbdx = T(80.0) * cos(x) + T(3.0) * pow(x, T(2.0));

  T c = s(x, p, w);
  T dcdx = dsdx(x, p, w);

  return dadx * (T(1.0) - c) - a * dcdx + dbdx * c + b * dcdx;
}

// *************************** Finite Differences *************************** //

/******************************************************************************
 * @brief Calculate the one-sided finite differences gradient of the
 *        smoothed function.
 *
 * @param[in] x Input value x.
 * @param[in] w Smoothing factor.
 * @param[in] alternative_h Whether to use the alternative step size.
 * @returns The one-sided finite differences of the smoothed primal function.
 ******************************************************************************/
template <typename T, typename PT>
T dfsdx_fd1(T x, const PT w, const bool alternative_h = false) {
  T eps = sqrt(numeric_limits<T>::epsilon());
  T h = (x == 0) ? eps : eps * fabs(x);

  // Alternative step size
  if (alternative_h) {
    h = eps * (T(1.0) + fabs(x));
    h = pow(T(2.0), round(log(h) / log(T(2.0))));
  }

  return (fs(x + h, w) - fs(x, w)) / h;
}

/******************************************************************************
 * @brief Calculate the central finite differences gradient of the
 *        smoothed function.
 *
 * @param[in] x Input value x.
 * @param[in] w Smoothing factor.
 * @param[in] alternative_h Whether to use the alternative step size.
 * @returns The central finite differences of the smoothed primal function.
 ******************************************************************************/
template <typename T, typename PT>
T dfsdx_fd2(T x, const PT w, const bool alternative_h = false) {
  T eps = cbrt(numeric_limits<T>::epsilon());
  T h = (x == 0) ? eps : eps * fabs(x);

  // Alternative step size
  if (alternative_h) {
    h = eps * (T(1.0) + fabs(x));
    h = pow(T(2.0), round(log(h) / log(T(2.0))));
  }

  return (fs(x + h, w) - fs(x - h, w)) / (2 * h);
}

// ******************************** Tangents ******************************** //

/******************************************************************************
 * @brief Calculate the tangent gradient of the smoothed function.
 *
 * @param[in] x Input value x.
 * @param[in] w Smoothing factor.
 * @returns The tangent of the smoothed primal function.
 ******************************************************************************/
template <typename T, typename PT>
void dfsdx_tangent(const T x, const PT w, T &dx) {
  using DCO_T = typename dco::gt1s<T>::type;
  DCO_T var_of_interest;

  dco::value(var_of_interest) = x;
  dco::derivative(var_of_interest) = 1.0;   // seeding
  var_of_interest = fs(var_of_interest, w); // evaluate
  dx = dco::derivative(var_of_interest);    // harvesting
}

/******************************************************************************
 * @brief Calculate the tangent gradient of the original function.
 *
 * @param[in] x Input value x.
 * @returns The tangent of the smoothed primal function.
 ******************************************************************************/
template <typename T> void dfdx_tangent(const T x, T &dx) {
  using DCO_T = typename dco::gt1s<T>::type;
  DCO_T var_of_interest;

  dco::value(var_of_interest) = x;
  dco::derivative(var_of_interest) = 1.0; // seeding
  var_of_interest = f(var_of_interest);   // evaluate
  dx = dco::derivative(var_of_interest);  // harvesting
}

/******************************************************************************
 * @brief Calculate the symbolic tangent gradient of the original function.
 *
 * @param[in] x Input value x.
 * @param[in] x_t1 Directional derivative x^(1).
 * @returns The tangent of the smoothed primal function.
 ******************************************************************************/
template <typename T> T dfdx_symbolic_tangent(const T x, const T x_t1) {
  return x < 0 ? 0.0 : 3 * pow(x, 2) * x_t1 + 80 * cos(x) * x_t1;
}

// ********************************** Main ********************************** //

int main(const int argc, const char *argv[]) {
  using T = double;
  using PT = double;

  assert(argc <= 5);

  // Smoothing factor
  PT w = 0.05;
  if (argc > 1) {
    w = static_cast<PT>(atof(argv[1]));
  }

  // Plotting range [lb, ub]
  T lb = -5;
  if (argc > 2) {
    lb = static_cast<PT>(atof(argv[2]));
  }

  T ub = 5;
  if (argc > 3) {
    ub = static_cast<PT>(atof(argv[3]));
  }
  assert(ub > lb);

  // Samples
  int32_t n = 1000;
  if (argc > 4) {
    n = static_cast<int32_t>(atoi(argv[4]));
  }

  T plotting_step = (ub - lb) / static_cast<T>(n - 1);

  for (T x = lb; x <= ub; x += plotting_step) {
    T dfsdx_fd2_res = dfsdx_fd2(x, w);
    T dfsdx_t1;
    dfsdx_tangent(x, w, dfsdx_t1);
    T dfdx_t1;
    dfdx_tangent(x, dfdx_t1);
    T dfdx_symb_t1 = dfdx_symbolic_tangent(x, 1.0);

    cout << x;
    cout << " " << dfsdx_fd2_res;
    cout << " " << dfsdx_t1;
    cout << " " << dfdx_t1;
    cout << " " << dfdx_symb_t1;
    cout << endl;
  }

  return 0;
}

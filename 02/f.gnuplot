#! /usr/bin/env gnuplot

plot 'data_10.dat' using 1:2 with lines title "dfdx (original)"
replot 'data_10.dat' using 1:3 with lines title "dfdx (w = 10)"
replot 'data_5.dat' using 1:3 with lines title "dfdx (w = 5)"
replot 'data_1.dat' using 1:3 with lines title "dfdx (w = 1)"
replot 'data_05.dat' using 1:3 with lines title "dfdx (w = 0.5)"
replot 'data_01.dat' using 1:3 with lines title "dfdx (w = 0.1)"
replot 'data_005.dat' using 1:3 with lines title "dfdx (w = 0.05)"
replot 'data_001.dat' using 1:3 with lines title "dfdx (w = 0.01)"

unset multiplot

pause mouse close

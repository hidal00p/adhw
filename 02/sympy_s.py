#! /usr/bin/env python3

from sympy import init_printing,  ccode, diff
from sympy import Function, Symbol, sin

DELIM = "=" * 5

init_printing()

c_dot = Symbol('c_dot')
x = Function('x')(c_dot)

f1 = Function('f1')
f1 = 1

f2 = Function('f2')
f2 = 80 * sin(x) + x ** 3 + 1

print(DELIM + "Seminar 2" + DELIM)
print(diff(x, c_dot))
print(diff(f1, c_dot))
print(diff(f2, c_dot))

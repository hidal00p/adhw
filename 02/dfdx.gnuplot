#! /usr/bin/env gnuplot

plot 'data.dat' using 1:2 with lines title "FD dfsdx"
replot 'data.dat' using 1:3 with lines title "1 Tangent dfsdx"
replot 'data.dat' using 1:4 with lines title "1 Tangent dfdx"

pause mouse close

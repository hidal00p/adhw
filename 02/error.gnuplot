#! /usr/bin/env gnuplot

set log y

plot 'data_01.dat' using 1:7 with lines title "fd1 error (w = 0.1)"
replot 'data_01.dat' using 1:9 with lines title "fd2 error (w = 0.1)"
replot 'data_01.dat' using 1:11 with lines title "fd1 error (w = 0.1, alternative h)"
replot 'data_01.dat' using 1:13 with lines title "fd2 error (w = 0.1, alternative h)"

pause mouse close

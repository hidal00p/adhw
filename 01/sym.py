from sympy import init_printing, symbols, ccode, diff, latex
from sympy import pprint, Function, sin

init_printing()

x, p = symbols('x p')
f = symbols('f', cls=Function)
f = (x ** p) * sin(x) + x ** 3

pprint(f)
print(ccode(f))
print(ccode(diff(f, x)))
print(latex(diff(f, x)))

// ************************************************************************** //
//       Introduction to Algorithmic Differentiation SS23 - Tutorial 1        //
//                                                                            //
//                   Finite Differences & Sigmoid Smoothing                   //
//                                                                            //
// Copyright (C) 2023 Software and Tools for Computational Engineering (STCE) //
//               RWTH Aachen University - www.stce.rwth-aachen.de             //
// ************************************************************************** //

// Include C++ standard header
#include <algorithm>
#include <array>
#include <cmath>
#include <iostream>

// ***************************** Primal function **************************** //

/******************************************************************************
 * @brief Calculate the (non-differentiable) primal function.
 *
 * @param[in] x Input value x.
 * @returns Result of primal function.
 ******************************************************************************/
template <typename T> T f(T x) { return fmax(80 * sin(x) + pow(x, 3) + 1, 1); }

// ******************************* Smoothing ******************************** //

/******************************************************************************
 * @brief Calculate sigmoid function dependent on input value, position of the
 *        non-differentiability and the smoothing factor.
 *
 * @param[in] x Input value x
 * @param[in] p Position of the non-differentiability.
 * @param[in] w Smoothing factor.
 * @returns Result of sigmoid function.
 ******************************************************************************/
template <typename T, typename PT> T s(T x, const PT p, const PT w) {
  return 1.0 / (1.0 + exp(-(x - p) / w));
}

/******************************************************************************
 * @brief Calculate smoothed function dependent on input value and the
 *        smoothing factor.
 *
 * @param[in] x Input value x
 * @param[in] w Smoothing factor.
 * @returns Result of smoothed primal function.
 ******************************************************************************/
template <typename T, typename PT> T fs(T x, const PT w) {
  T a = 1.0;
  T b = 80 * sin(x) + pow(x, 3) + 1;
  T sigma = s(x, 0.0, w);

  return (1 - sigma) * a + sigma * b;
}

// ************************** Analytical Gradients ************************** //

/******************************************************************************
 * @brief Calculate the analytical gradient of the primal function.
 *
 * @param[in] x Input value x
 * @returns The analytical gradient of the primal function.
 ******************************************************************************/
template <typename T> T dfdx(T x) {
  return x > 0 ? 80 * cos(x) + 3 * pow(x, 2) : 0;
}

/******************************************************************************
 * @brief Calculate the analytical gradient of the sigmoid function.
 *
 * @param[in] x Input value x
 * @param[in] p Position of the non-differentiability.
 * @param[in] w Smoothing factor.
 * @returns The analytical gradient of the sigmoid function.
 ******************************************************************************/
template <typename T, typename PT> T dsdx(T x, const PT p, const PT w) {
  T v = exp(-(x - p) / w);
  T dvdx = -v / w;
  T dsdv = -1.0 / pow(1.0 + v, 2);

  return dsdv * dvdx;
}

/******************************************************************************
 * @brief Calculate the analytical gradient of the smoothed function.
 *
 * @param[in] x Input value x
 * @param[in] w Smoothing factor.
 * @returns The analytical gradient of the smoothed primal function.
 ******************************************************************************/
template <typename T, typename PT> T dfsdx(T x, const PT w) {
  T _s = s(x, 0.0, w);
  T _dsdx = dsdx(x, 0.0, w);

  T _f = 80 * sin(x) + pow(x, 3) + 1;
  T _dfdx = 80 * cos(x) + 3 * pow(x, 2);

  return _dsdx + _dsdx * _f + _s * _dfdx;
}

// *************************** Finite Differences *************************** //

/******************************************************************************
 * @brief Calculate the one-sided finite differences gradient of the
 *        smoothed function.
 *
 * @param[in] x Input value x
 * @param[in] w Smoothing factor.
 * @returns The one-sided finite differences of the smoothed primal function.
 ******************************************************************************/
template <typename T, typename PT> T dfsdx_fd1(T x, const PT w) {
  T epsilon = std::numeric_limits<T>::epsilon();
  T dx = (x == 0) ? sqrt(epsilon) : sqrt(epsilon) * fabs(x);
  return (fs(x + dx, w) - fs(x, w)) / dx;
}

/******************************************************************************
 * @brief Calculate the central finite differences gradient of the
 *        smoothed function.
 *
 * @param[in] x Input value x
 * @param[in] w Smoothing factor.
 * @returns The central finite differences of the smoothed primal function.
 ******************************************************************************/
template <typename T, typename PT> T dfsdx_fd2(T x, const PT w) {
  T machine_precision = std::numeric_limits<T>::epsilon();
  T dx = (x == 0) ? sqrt(machine_precision) : sqrt(machine_precision) * fabs(x);
  return (fs(x + dx, w) - fs(x - dx, w)) / 2 / dx;
}

// ********************************** Main ********************************** //

int main() {
  using T = double;
  using PT = double;

  PT w = 0.025;

  for (int32_t i = -50; i <= 500; i++) {
    T x = 0.001 * i;
    T f_res = f(x);
    T fs_res = fs(x, w);
    T dfdx_res = dfdx(x);
    T dfsdx_res = dfsdx(x, w);
    T dfsdx_fd1_res = dfsdx_fd1(x, w);
    T dfsdx_fd2_res = dfsdx_fd2(x, w);

    std::cout << x;
    std::cout << " " << f_res;
    std::cout << " " << fs_res;
    std::cout << " " << dfdx_res;
    std::cout << " " << dfsdx_res;
    std::cout << " " << dfsdx_fd1_res;
    std::cout << " "
              << std::fabs(dfsdx_fd1_res - dfsdx_res) / std::fabs(dfsdx_res);
    std::cout << " " << dfsdx_fd2_res;
    std::cout << " "
              << std::fabs(dfsdx_fd2_res - dfsdx_res) / std::fabs(dfsdx_res);
    std::cout << std::endl;
  }

  return 0;
}

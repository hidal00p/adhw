// ************************************************************************** //
//       Introduction to Algorithmic Differentiation SS23 - Tutorial 6        //
//                                                                            //
//             First Order Adjoint Mode & Differential Invariant              //
//                                                                            //
// Copyright (C) 2023 Software and Tools for Computational Engineering (STCE) //
//               RWTH Aachen University - www.stce.rwth-aachen.de             //
// ************************************************************************** //

// Include Eigen
#include <Eigen/Dense>
#include <dco.hpp>

// Include C++ standard header
#include <algorithm>
#include <array>
#include <cassert>
#include <chrono>
#include <cmath>
#include <iostream>

// ****************************** Custom types ****************************** //

//! Vector (inputs & Gradient)
template <typename T> using vec_t = Eigen::VectorX<T>;

//! Matrix (Hessian)
template <typename T> using mat_t = Eigen::MatrixX<T>;

// ***************************** Primal function **************************** //

/******************************************************************************
 * @brief Evaluate the Rosenbrock function.
 *
 * @param[in] x Input vector x.
 * @returns Result of primal function.
 ******************************************************************************/
template <typename T> T R(const vec_t<T> &x) {
  T a = 1.0;
  T b = 100.0;

  T result = 0;
  for (size_t i = 0; i < x.size() - 1; i++) {
    result += pow(a - x(i), 2.0) + b * pow(x(i + 1) - pow(x(i), 2.0), 2.0);
  }

  return result;
}

// ******************************** Tangents ******************************** //

/******************************************************************************
 * @brief Calculate the tangent Gradient of the Rosenbrock function.
 *
 * @param[in] x Input vector x.
 * @returns The tangent Gradient of the Rosenbrock function.
 ******************************************************************************/
template <typename T> vec_t<T> dRdx_tangent(vec_t<T> x) {
  vec_t<T> g = vec_t<T>::Zero(x.size());

  using tangent_t = typename dco::gt1s<T>::type;

  vec_t<tangent_t> x_t;
  dco::value(x_t) = x;

  for (size_t i = 0; i < x.size(); i++) {
    dco::derivative(x_t(i)) = 1.0;
    tangent_t y_t = R(x_t);
    g(i) = dco::derivative(y_t);
    dco::derivative(x_t(i)) = 0.0;
  }

  return g;
}

/******************************************************************************
 * @brief One tangent evaluation of the Rosenbrock function.
 *
 * @param[in] x Input vector x.
 * @param[in] x_t1 Directional derivative x^(1) (seed).
 * @returns The tangent of the Rosenbrock function.
 ******************************************************************************/
template <typename T> T symbolic_tangent(vec_t<T> x, vec_t<T> x_t1) {
  using tangent_t = typename dco::gt1s<T>::type;

  vec_t<tangent_t> x_t;
  dco::value(x_t) = x;
  for (size_t i = 0; i < x.size(); i++) {
    dco::derivative(x_t(i)) = x_t1(i);
  }

  tangent_t y_t = R(x_t);
  return dco::derivative(y_t);
}

/******************************************************************************
 * @brief Calculate the tangent Gradient of the Rosenbrock function using
 *        vector mode.
 *
 * @param[in] x Input vector x.
 * @returns The tangent Gradient of the Rosenbrock function.
 ******************************************************************************/
template <typename T> vec_t<T> dRdx_tangent_vector(vec_t<T> x) {
  vec_t<T> g = vec_t<T>::Zero(x.size());

  // TODO: Task 4 (optional)

  return g;
}

// ******************************** Adjoints ******************************** //

/******************************************************************************
 * @brief Calculate the adjoint Gradient of the Rosenbrock function.
 *
 * @param[in] x Input vector x.
 * @param[in] y_a1 Directional derivative y_(1) (seed).
 * @returns The adjoint Gradient of the Rosenbrock function.
 ******************************************************************************/
template <typename T> vec_t<T> dRdx_adjoint(vec_t<T> x, T y_a1 = 1.0) {
  using method_type = typename dco::ga1s<T>;
  using adjoint_type = typename method_type::type;

  // initialize a gradient vector
  vec_t<T> g = vec_t<T>::Zero(x.size());

  // initializer overloaded set of adjoint input parameters
  vec_t<adjoint_type> X;
  X = x;

  // initialize adjoint recording tape
  dco::smart_tape_ptr_t<method_type> tape;
  tape->register_variable(X);

  adjoint_type y = R(X);

  // seed
  dco::derivative(y) = y_a1;
  tape->interpret_adjoint();

  // harvest
  for (int i = 0; i < X.size(); ++i)
    g[i] = dco::derivative(X[i]);

  return g;
}

// ******************************* Validation ******************************* //

/******************************************************************************
 * @brief Validate the differential identity (tangent-adjoint identity).
 *
 * @param[in] x_t1 Directional derivative x^(1) (seed).
 * @param[in] x_a1 Directional derivative x_(1) (harvested).
 * @param[in] y_t1 Directional derivative y^(1) (harvested).
 * @param[in] y_a1 Directional derivative y_(1) (seed).
 * @returns The adjoint Gradient of the Rosenbrock function.
 ******************************************************************************/
template <typename T>
void differential_identity(vec_t<T> x_t1, vec_t<T> x_a1, T y_t1, T y_a1) {
  /*
   * Since both calls were randomly seeded, in order to validate the
   * indentity we need to make sure that there is some common value, which
   * is equivalent irregardless of the direction chosen.
   * i.e.
   * Adjoint: x_(1) = {y_x1*y_(1), y_x2*y_(1), ...}
   * Tanget:  y^(1) = y_x1*x^(1) + y_x2*x^(1) + ...
   *
   * Hence, we can compare the following:
   * dot(x_(1), x^(1)) ? y^(1)*y_(1)
   */

  T epsilon = std::sqrt(std::numeric_limits<T>::epsilon());
  T Dy_a = x_a1.transpose() * x_t1;
  T Dy_t = y_t1 * y_a1;

  if (std::abs(Dy_t - Dy_a) > epsilon)
    std::cout << "Differentials *DO NOT* match :thumbsdown:" << std::endl;
  else
    std::cout << "Differentials match :thumbsup:" << std::endl;

  std::cout << "Dy_a: " << Dy_a << std::endl << "Dy_t: " << Dy_t << std::endl;
}

// ********************************** Main ********************************** //

template <typename Clock>
static inline double lap(std::chrono::time_point<Clock> start) {
  return std::chrono::duration_cast<std::chrono::nanoseconds>(Clock::now() -
                                                              start)
             .count() /
         1000.0;
}

int main(const int argc, const char *argv[]) {
  using T = double;

  assert(argc <= 4);
  auto start = std::chrono::high_resolution_clock::now();

  // Input size
  size_t n = 10;
  if (argc > 1) {
    n = static_cast<size_t>(std::stoull(argv[1]));
  }

  // Iterations
  size_t iterations = 1000;
  if (argc > 2) {
    iterations = static_cast<size_t>(std::stoull(argv[2]));
  }

  // Warmup
  size_t warmup = 5;
  if (argc > 3) {
    warmup = static_cast<size_t>(std::stoull(argv[3]));
  }

  // Random initial values, and tangent and adjoint seeds
  vec_t<T> x0 = vec_t<T>::Random(n);
  vec_t<T> x0_t1 = vec_t<T>::Random(n);
  T y0_a1 = vec_t<T>::Random(1)[0];

  // Validate differential identity
  T y0_t1 = symbolic_tangent(x0, x0_t1);
  vec_t<T> x0_a1 = dRdx_adjoint(x0, y0_a1);
  differential_identity(x0_t1, x0_a1, y0_t1, y0_a1);

  // Passive timings
  T y;
  double passive_time = 0.0;
  for (size_t i = 0; i < warmup; i++) {
    y = R(x0);
  }
  for (size_t i = 0; i < iterations; i++) {
    start = std::chrono::high_resolution_clock::now();
    y = R(x0);
    passive_time += lap(start);
  }
  passive_time /= iterations;

  std::cout << "y = " << y << "\n";
  std::cout << "Passive time [μs]: " << passive_time << "\n\n";

  // Tangent gradient timings
  vec_t<T> dRdx_t;
  double tangent_time = 0.0;
  for (size_t i = 0; i < warmup; i++) {
    dRdx_t = dRdx_tangent(x0);
  }
  for (size_t i = 0; i < iterations; i++) {
    start = std::chrono::high_resolution_clock::now();
    dRdx_t = dRdx_tangent(x0);
    tangent_time += lap(start);
  }
  tangent_time /= iterations;

  std::cout << "norm(dFdx_t) = " << dRdx_t.norm() << "\n";
  std::cout << "Tangent time [μs]: " << tangent_time;
  std::cout << " (factor: " << tangent_time / passive_time << ")\n\n";

  // Adjoint gradient timings
  vec_t<T> dRdx_a;
  double adjoint_time = 0.0;
  for (size_t i = 0; i < warmup; i++) {
    dRdx_a = dRdx_adjoint(x0);
  }
  for (size_t i = 0; i < iterations; i++) {
    start = std::chrono::high_resolution_clock::now();
    dRdx_a = dRdx_adjoint(x0);
    adjoint_time += lap(start);
  }
  adjoint_time /= iterations;

  std::cout << "norm(dFdx_a) = " << dRdx_a.norm();
  std::cout << " (error = " << (dRdx_t - dRdx_a).norm() << ")\n";
  std::cout << "Adjoint time [μs]: " << adjoint_time;
  std::cout << " (factor: " << adjoint_time / passive_time << ")\n\n";

  // Tangent vector gradient timings
  vec_t<T> dRdx_tv;
  double tangent_vector_time = 0.0;
  for (size_t i = 0; i < warmup; i++) {
    dRdx_tv = dRdx_tangent_vector(x0);
  }
  for (size_t i = 0; i < iterations; i++) {
    start = std::chrono::high_resolution_clock::now();
    dRdx_tv = dRdx_tangent_vector(x0);
    tangent_vector_time += lap(start);
  }
  tangent_vector_time /= iterations;

  std::cout << "norm(dRdx_tv) = " << dRdx_tv.norm();
  std::cout << " (error = " << (dRdx_t - dRdx_tv).norm() << ")\n";
  std::cout << "Tangent vector time [μs]: " << tangent_vector_time;
  std::cout << " (factor: " << tangent_vector_time / passive_time << ")\n";

  return 0;
}

### Runtime comparison

For lower dimensional inputs tangent method outperforms adjoint. As the dimension grows larger
adjoints starts giving significantly better results, pretty quickly.
Another interesting observaton is that tangent mode runtime grows with O(N*N),
while adjoint with O(N)

**Data points:**
| n | T [msec] | A [msec] |
| - | -------- | -------- |
| 1 | 0.97 | 23.6 |
| 2 | 3.1 | 27 |
| 3 | 6.9 | 28.4 |
| 4 | 12.5 | 32.1 |
| 16 | 199.6 | 54.5 |
| 64 | 3192 | 139.8 |
| 128 | 13009 | 257.5 |

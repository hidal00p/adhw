// ************************************************************************** //
//       Introduction to Algorithmic Differentiation SS23 - Tutorial 5        //
//                                                                            //
//                           Higher Order Tangents                            //
//                                                                            //
// Copyright (C) 2023 Software and Tools for Computational Engineering (STCE) //
//               RWTH Aachen University - www.stce.rwth-aachen.de             //
// ************************************************************************** //

// Include Eigen & dco
#include <Eigen/Dense>
#include <dco.hpp>

// Include C++ standard header
#include <algorithm>
#include <array>
#include <cassert>
#include <cmath>
#include <iostream>

#define D dco::derivative
#define V dco::value

using namespace std;

// ****************************** Custom types ****************************** //

//! Vector with 2 entries (inputs & Gradient)
template <typename T> using vec2_t = Eigen::Vector2<T>;

// ***************************** Primal function **************************** //

/******************************************************************************
 * @brief Evaluate the lighthouse function.
 *
 * @param[in] nu Distance between the lighthouse and the quay on the x-axis.
 * @param[in] gamma Slope of the quay in relation to the lighthouse.
 * @param[in] omega Angular velocity of the light beam.
 * @param[in] t Time t.
 * @returns Coordinates of the point where the light hits the quay.
 ******************************************************************************/
template <typename T>
vec2_t<T> lighthouse(const T &nu, const T &gamma, const T &omega, const T &t) {
  // Root nodes
  T v0 = nu;
  T v1 = gamma;
  T v2 = omega;
  T v3 = t;

  T v4 = v2 * v3;
  T v5 = tan(v4);
  T v6 = v0 * v5;
  T v7 = v1 - v5;

  T v8 = v6 / v7;
  T v9 = v1 * v8;
  return {v8, v9};
}

// ******************************** Tangents ******************************** //

/******************************************************************************
 * @brief Calculate the fourth order partial derivative d^4 y / dnu dgamma dt^2.
 *
 * @param[in] nu Distance between the lighthouse and the quay on the x-axis.
 * @param[in] gamma Slope of the quay in relation to the lighthouse.
 * @param[in] omega Angular velocity of the light beam.
 * @param[in] t Time t.
 * @returns d^4 y / dnu dgamma dt^2.
 ******************************************************************************/
template <typename K>
K d4y_dnu_dgamma_dt2(const K &nu, const K &gamma, const K &omega, const K &t) {
  using DCO_4T = typename dco::gt1s<typename dco::gt1s<
      typename dco::gt1s<typename dco::gt1s<K>::type>::type>::type>::type;

  // Init
  DCO_4T N = nu;
  DCO_4T G = gamma;
  DCO_4T O = omega;
  DCO_4T T = t;

  // Seed
  D(V(V(V(N)))) = 1.0; // Pick out dF_dnu tensor
  D(V(V(V(G)))) = 0.0;
  D(V(V(V(O)))) = 0.0;
  D(V(V(V(T)))) = 0.0;

  V(D(V(V(N)))) = 0.0;
  V(D(V(V(G)))) = 1.0; // Pick out d2F_dnu_dgamma tensor
  V(D(V(V(O)))) = 0.0;
  V(D(V(V(T)))) = 0.0;

  V(V(D(V(N)))) = 0.0;
  V(V(D(V(G)))) = 0.0;
  V(V(D(V(O)))) = 0.0;
  V(V(D(V(T)))) = 1.0; // Pick out d3F_dnu_dgamma_dt matrix

  V(V(V(D(N)))) = 0.0;
  V(V(V(D(G)))) = 0.0;
  V(V(V(D(O)))) = 0.0;
  V(V(V(D(T)))) = 1.0; // Pick out d4F_dnu_dgamma_d2t direction

  vec2_t<DCO_4T> F = lighthouse(N, G, O, T);

  // Extract the desired y(4) all the way down the derivative path
  return D(D(D(D(F[1]))));
}

// ********************************** Main ********************************** //

int main(const int argc, const char *argv[]) {
  using T = double;

  const T pi = 3.14159265358979323846;

  T nu = 100.0;
  T gamma = pi / 6.0;
  T omega = pi / 15.0;
  T t = 1.0;

  std::cout << lighthouse(nu, gamma, omega, t) << std::endl;
  std::cout << d4y_dnu_dgamma_dt2(nu, gamma, omega, t) << std::endl;

  return 0;
}

### 1 Tangent
SAC
```text
v4^(1) = v3*v2^(1)
v4^(1) += v2*v3^(1)

tmp = cos(v4)
v5^(1) = v4^(1) / (tmp*tmp)

v6^(1) = v5*v0^(1)
v6^(1) += v0*v5^(1)

v7^(1) = v1^(1) - v5^(1)

v8^(1) = v6^(1) / v7
v8^(1) += -v6*v7^(1) / (v7*v7)

v9^(1) = v8*v1^(1)
v9^(1) += v1*v0^(1)
```

See DAG [here](dag/1t_dag.png)

### 2 Tangent
The partial computation done for the 2 order tangent SAC are the same as for the
1 order tangent, so please refer to it above. In the following only the unique
computations for the 2 order SAC are shown.

SAC
```text
v4^(1, 2) = v3^(2)*v2^(1) + v3*v2^(1, 2)
v4^(1) += v2^(2)*v3^(1) + v2*v3^(1, 2)

tmp = cos(v4)
dtmp = sin(v4)
v5^(1, 2) = v4^(1, 2) / (tmp*tmp) + 2*v4^(1)*v4^(2)*tmp / (dtmp*dtmp*dtmp)

v6^(1, 2) = v5^(2)*v0^(1) + v5*v0^(1, 2)
v6^(1, 2) += v0^(2)*v5^(1) + v0*v5^(1, 2)

v7^(1, 2) = v1^(1, 2) - v5^(1, 2)

v8^(1, 2) = v6^(1, 2) / v7 - v6^(1)*v7^(2) / (v7*v7)
v8^(1) += -v6^(2)*v7^(1) / (v7*v7) - v6*v7^(1, 2) / (v7*v7) + 2*v6*v7^(1)*v7^(2) / (v7*v7*v7)

v9^(1, 2) = v8^(2)*v1^(1) + v8*v1^(1, 2)
v9^(1, 2) += v1^(2)*v0^(1) + v1*v0^(1, 2)
```

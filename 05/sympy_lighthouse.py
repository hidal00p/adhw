#! /usr/bin/env python3

from sympy import init_printing, symbols, ccode, diff, tan

init_printing()

ν, γ, ω, t = symbols('nu gamma omega t')

x = ν * tan(ω * t) / (γ - tan(ω * t))
y = γ * ν * tan(ω * t) / (γ - tan(ω * t))

print(ccode(x))
print(ccode(y))
print()


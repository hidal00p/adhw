SRC_FILES = $(shell find . -maxdepth 2 \( -name "*.cpp" -o -name "*.h" \) | xargs)

fmt: $(SRC_FILES)
	@clang-format -i $(SRC_FILES)

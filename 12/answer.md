# Exploitation of sparsity 2.0
In order to exploit the sparsity of the Jacobian, it
is necessary first to compress the Jacobian or a Hessian.

Once the compressed form is known, a suitable more efficient
seed matrix can be generated for harvesting the derivatives.

The matrix compression problem is equivalent to graph coloring
problem, hence the following steps need to be carried to be
able to compress the sparse matrix:

1. Construct the graph
2. Order the vertices
3. Color the graph
4. Reflect the colors back to the matrix


## 1. Graph construction
### J
* Column intersection graph [G_c](./graphs/G_c.png)
* Row intersection graph [G_r](./graphs/G_r.png)
* Bipartite graph [G_b](./graphs/G_b.png)

### H
* Adjacency graph [G_a](./graphs/G_a.png)


## 2. Vertex ordering & Graph coloring
Using column compression the following coloring is obtained for each
corresponding ordering method:

* Largest first. A: {3}, B: {0, 4}, C: {1, 2, 5}
  - 3, 0, 4, 1, 2, 5
* Smallest last. A: {1, 2, 5}, B: {0, 4}, C: {3}
  - 5, 4, 3, 2, 0, 1
* Incidence. A: {0, 4}, B: {1, 2, 5}, C: {3}
  - 0, 1, 3, 2, 4, 5


## 3. Seed matrix
### J
* Tangent mode corresponding to one of column compressions:
```text
|1 0 0|
|0 1 0|
|0 1 0|
|0 0 1|
|1 0 0|
|0 1 0|
```

### H

// ************************************************************************** //
//       Introduction to Algorithmic Differentiation SS23 - Tutorial 8        //
//                                                                            //
//                          Handwritten Tangent Code                          //
//                                                                            //
// Copyright (C) 2023 Software and Tools for Computational Engineering (STCE) //
//               RWTH Aachen University - www.stce.rwth-aachen.de             //
// ************************************************************************** //

// Include Eigen
#include <Eigen/Dense>
#include <dco.hpp>

// Include C++ standard header
#include <algorithm>
#include <array>
#include <cassert>
#include <chrono>
#include <cmath>
#include <iostream>

using namespace std;

// *********************** Euler-Maruyama Parameters ************************ //

// Time interval we want to solve: [0, T]
constexpr double T = 1.0;

// ****************************** Custom Types ****************************** //

//! Vector (inputs & Gradient)
template <typename T> using vec_2t = Eigen::Vector2<T>;

template <typename T> using vec_4t = Eigen::Vector4<T>;

template <typename T> using vec_t = Eigen::VectorX<T>;

//! Matrix (Hessian)
template <typename T> using mat_t = Eigen::MatrixX<T>;

template <typename T> using adjoint_m = dco::ga1s<T>;

template <typename T> using adjoint_t = typename dco::ga1s<T>::type;

template <typename T> using tangent_t = typename dco::gt1s<T>::type;

// ******************************** Primals ********************************* //

// Function f in the SDE
template <typename AT, typename PT>
AT f(const size_t i, const AT &x, const vec_t<AT> &p, const PT &t) {
  return p(i) * sin(x * t);
}

// Function g in the SDE
template <typename AT, typename PT>
AT g(const size_t i, const AT &x, const vec_t<AT> &p, const PT &t) {
  return p(i) * cos(x * t);
}

// Calculate a single path
template <typename AT, typename PT>
AT single_path(const int j, const size_t ns, const PT &x0, const vec_t<AT> &p,
               const mat_t<PT> &dW) {
  // Init state variables
  AT x = x0;
  PT t = 0;

  // Time step
  PT dt = T / ns;

  // Perform steps
  for (size_t i = 0; i < ns; i++) {
    x += f(i, x, p, t) * dt + g(i, x, p, t) * dW(j, i);
    t += dt;
  }

  return x;
}

// Calculate expected value (average) over all paths
template <typename AT, typename PT>
AT euler_maruyama(const size_t np, const size_t ns, const PT &x0,
                  const vec_t<AT> &p, const mat_t<PT> &dW) {
  AT expected_value = 0;
  for (size_t j = 0; j < np; j++) {
    expected_value += single_path(j, ns, x0, p, dW);
  }
  expected_value /= np;

  return expected_value;
}

// Hand-written Tangent Methods

/*
 * hwt1 - hand-written tangent 1(st order)
 */
namespace hwt1 {
template <typename AT, typename PT>
void f(vec_2t<AT> &F, const AT &x, const AT &x_T, const AT &p, const AT &p_T,
       const PT &t) {
  AT v0 = sin(x * t);
  AT v1 = cos(x * t);
  F[0] = p * v0;
  F[1] = v0 * p_T + p * v1 * t * x_T;
}

template <typename AT, typename PT>
void g(vec_2t<AT> &G, const AT &x, const AT &x_T, const AT &p, const AT &p_T,
       const PT &t) {
  AT v0 = cos(x * t);
  AT v1 = sin(x * t);
  G[0] = p * v0;
  G[1] = v0 * p_T - p * v1 * t * x_T;
}

template <typename AT, typename PT>
void single_path(vec_2t<AT> &E_ctx, const int j, const size_t ns, const PT &x0,
                 const vec_t<AT> &p, const vec_t<AT> &p_T,
                 const mat_t<PT> &dW) {
  // Init state variables
  E_ctx[0] = x0, E_ctx[1] = 0.0;
  vec_2t<AT> F, G;
  PT t = 0;

  // Time step
  PT dt = T / ns;

  // Perform steps
  for (size_t i = 0; i < ns; i++) {
    f(F, E_ctx[0], E_ctx[1], p[i], p_T[i], t);
    g(G, E_ctx[0], E_ctx[1], p[i], p_T[i], t);

    E_ctx[0] += F[0] * dt + G[0] * dW(j, i);
    E_ctx[1] += F[1] * dt + G[1] * dW(j, i);

    t += dt;
  }
}

/*
 * euler_maruyama: Intraprocedural handwritten SAC for 1st order tangent.
 */
template <typename AT, typename PT>
vec_2t<AT> euler_maruyama(const size_t np, const size_t ns, const PT &x0,
                          const vec_t<AT> &p, const vec_t<AT> &p_T,
                          const mat_t<PT> &dW) {

  AT E = 0.0, E_T = 0.0;
  vec_2t<AT> E_ctx;
  for (size_t j = 0; j < np; j++) {
    single_path(E_ctx, j, ns, x0, p, p_T, dW);
    E += E_ctx[0];
    E_T += E_ctx[1];
  }
  E /= np;
  E_T /= np;

  return {E, E_T};
}

template <typename PT>
vec_t<PT> euler_maruyama_gradient(const size_t np, const size_t ns,
                                  const PT &x0, const vec_t<PT> &p,
                                  const mat_t<PT> &dW) {

  // Initialize a vector to store the gradient dedp
  vec_t<PT> dedp = vec_t<PT>::Zero(p.size());

  // Initilaize memory to hold the seeds
  vec_t<PT> p_T = vec_t<PT>::Zero(p.size());

  for (int i = 0; i < p.size(); i++) {
    p_T[i] = 1.0; // Seed
    dedp[i] = euler_maruyama(np, ns, x0, p, p_T,
                             dW)[1]; // harvest. NOTE: that the value stored as
                                     // the 0th element is discarded.
    p_T[i] = 0.0;
  }

  return dedp;
}
} // namespace hwt1

/*
 * hwt2 - hand-written tangent 2(nd order)
 */
namespace hwt2 {
template <typename AT, typename PT>
void f(vec_2t<AT> &F, vec_2t<AT> &F1, vec_2t<AT> &x, vec_2t<AT> &x1,
       vec_2t<AT> &p, vec_2t<AT> &p1, const PT &t) {
  // y = p * sin(x * t) -> p.v.v * sin(x.v.v * t) -> p[0] * sin(x[0] * t)
  // dy = sin(x.v * t) * p.d + p.v * t * x.d * cos(x.v * t) -> sin(x[0] * t) *
  // p[2] + p[0] * t * x[2] * cos(x[0] * t) ddy = p.d.v * x.v.d * t * cos(x.v.v
  // * t)
  //     + t * x.d.v * cos(x.v.v * t) * p.v.d + p.v.v * t * cos(x.v.v * t) *
  //     x.d.d - p.v.v * t * x.d.v * t * x.v.d * sin(x.v.v * t)
  // -> p[2] * x[1] * t * cos(x[0] * t) + t * x[2] * cos(x[0] * t) * p[1] + p[0]
  // * t * cos(x[0] * t) * x.d.d - p[0] * t * x[2] * t * x[1] * sin(x[0] * t)

  AT v0 = sin(x[0] * t);
  AT v1 = t * cos(x[0] * t);
  AT v2 = -t * t * v0;

  F[0] = p[0] * v0;
  F[1] = v0 * p[1] + p[0] * v1 * x[1];

  F1[0] = v0 * p1[0] + p[0] * v1 * x1[0];
  F1[1] = p1[0] * v1 * x[1] + v0 * p1[1] + v1 * x1[0] * p[1] +
          p[0] * x1[0] * v2 * x[1] + p[0] * v1 * x1[1];
}

template <typename AT, typename PT>
void g(vec_2t<AT> &G, vec_2t<AT> &G1, vec_2t<AT> &x, vec_2t<AT> &x1,
       vec_2t<AT> &p, vec_2t<AT> &p1, const PT &t) {
  AT v0 = cos(x[0] * t);
  AT v1 = -t * sin(x[0] * t);
  AT v2 = -t * t * v0;

  G[0] = p[0] * v0;
  G[1] = v0 * p[1] + p[0] * v1 * x[1];

  G1[0] = v0 * p1[0] + p[0] * v1 * x1[0];
  G1[1] = p1[0] * v1 * x[1] + v0 * p1[1] + v1 * x1[0] * p[1] +
          p[0] * x1[0] * v2 * x[1] + p[0] * v1 * x1[1];
}

template <typename AT, typename PT>
void single_path(vec_2t<AT> &E_ctx, vec_2t<AT> &E1_ctx, const int j,
                 const size_t ns, const vec_t<AT> &p, const vec_t<AT> &p_T,
                 const vec_t<AT> &p1, const vec_t<AT> &p1_T,
                 const mat_t<PT> &dW) {
  // Init state variables
  vec_2t<AT> F, G, P;
  vec_2t<AT> F1, G1, P1;
  PT t = 0;

  // Time step
  PT dt = T / ns;

  // Perform steps
  for (size_t i = 0; i < ns; i++) {
    P[0] = p[i];
    P[1] = p_T[i];
    P1[0] = p1[i];
    P1[1] = p1_T[i];

    f(F, F1, E_ctx, E1_ctx, P, P1, t);
    g(G, G1, E_ctx, E1_ctx, P, P1, t);

    E_ctx[0] += F[0] * dt + G[0] * dW(j, i);
    E_ctx[1] += F[1] * dt + G[1] * dW(j, i);

    E1_ctx[0] += F1[0] * dt + G1[0] * dW(j, i);
    E1_ctx[1] += F1[1] * dt + G1[1] * dW(j, i);

    t += dt;
  }
}

/*
 * euler_maruyama: Intraprocedural handwritten SAC for 2nd order tangent.
 */
template <typename AT, typename PT>
vec_4t<AT> euler_maruyama(const size_t np, const size_t ns, const PT &x0,
                          const vec_t<AT> &p, const vec_t<AT> &p_T,
                          const vec_t<AT> &p1, const vec_t<AT> &p1_T,
                          const mat_t<PT> &dW) {

  AT E = 0.0, E_T = 0.0, E1 = 0.0, E1_T = 0.0;
  vec_2t<AT> E_ctx;
  vec_2t<AT> E1_ctx;

  for (size_t j = 0; j < np; j++) {
    E_ctx[0] = x0, E_ctx[1] = 0.0;
    E1_ctx[0] = 0.0, E1_ctx[1] = 0.0;

    single_path(E_ctx, E1_ctx, j, ns, p, p_T, p1, p1_T, dW);

    E += E_ctx[0];
    E_T += E_ctx[1];

    E1 += E1_ctx[0];
    E1_T += E1_ctx[1];
  }
  E /= np;
  E_T /= np;

  E1 /= np;
  E1_T /= np;

  return {E, E_T, E1, E1_T};
}
// Euler-Maruyama hessian driver
template <typename PT>
mat_t<PT> euler_maruyama_hessian(const size_t np, const size_t ns, const PT &x0,
                                 const vec_t<PT> &p, const mat_t<PT> &dW) {

  // Init Hessian
  mat_t<PT> ddedpp = mat_t<PT>::Zero(p.size(), p.size());

  vec_t<PT> p_T = vec_t<PT>::Zero(p.size());  // VD
  vec_t<PT> p1 = vec_t<PT>::Zero(p.size());   // DV
  vec_t<PT> p1_T = vec_t<PT>::Zero(p.size()); // DD

  for (int i = 0; i < p.size(); i++) {
    p_T[i] = 1.0;
    for (int j = 0; j <= i; j++) {
      p1[j] = 1.0;
      ddedpp(i, j) = euler_maruyama(np, ns, x0, p, p_T, p1, p1_T, dW)[3];
      ddedpp(j, i) = ddedpp(i, j);
      p1[j] = 0.0;
    }
    p_T[i] = 0.0;
  }

  return ddedpp;
}
} // namespace hwt2

// Euler-Maruyama driver returning passive result, Gradient and Hessian
template <typename PT>
std::tuple<PT, vec_t<PT>, mat_t<PT>>
euler_maruyama_combined_driver(const size_t np, const size_t ns, const PT &x0_p,
                               const vec_t<PT> &p_p, const mat_t<PT> &dW_p) {

  // Tangent over adjoint type
  using toa_t = adjoint_t<tangent_t<PT>>;

  // Init passive result, Gradient and Hessian
  PT e = 0;
  vec_t<PT> dedp = vec_t<PT>::Zero(p_p.size());
  mat_t<PT> dedpp = mat_t<PT>::Zero(p_p.size(), p_p.size());

  // Create tape
  dco::smart_tape_ptr_t<adjoint_m<tangent_t<PT>>> tape;

  // Active parameters
  vec_t<toa_t> p_toa = vec_t<toa_t>::Zero(p_p.size());
  for (size_t i = 0; i < ns; i++) {
    dco::passive_value(p_toa(i)) = p_p(i);
    tape->register_variable(p_toa(i));
  }

  // Store tape position
  auto tpos = tape->get_position();

  // Declare active result variable used in loop
  toa_t e_toa;

  for (size_t i = 0; i < ns; i++) {
    dco::derivative(dco::value(p_toa(i))) = 1.0; // seed tangent p^{(1)}

    // Forward and reverse pass
    e_toa = euler_maruyama(np, ns, x0_p, p_toa, dW_p);
    dco::value(dco::derivative(e_toa)) = 1.0; // seed adjoint e_{(2)}
    tape->interpret_adjoint();

    // Harvest first derivatives (tangents)
    dedp(i) = dco::derivative(dco::value(e_toa));

    // Harvest derivatives and reset afterwards
    for (size_t j = 0; j <= i; j++) {
      dedpp(i, j) =
          dco::derivative(dco::derivative(p_toa(j))); // harvest p^{(1)}_{(2)}
      dedpp(j, i) = dedpp(i, j);
    }

    // Reset tape so that the inputs are still registered
    tape->reset_to(tpos);
    tape->zero_adjoints(); // reset adjoints p_{(2)} and p^{(1)}_{(2)}
    dco::derivative(dco::value(p_toa(i))) = 0.0; // reset tangent p^{(1)}
  }

  // Harvest passive result
  e = dco::value(dco::value(e_toa));

  // Return passive result, Gradient and Hessian
  return std::make_tuple(e, dedp, dedpp);
}

// ************************************************************************** //

// Calibration driver using Newton's algorithm
template <typename PT>
vec_t<PT> parameter_calibration(const size_t np, const size_t ns, const PT &x0,
                                const vec_t<PT> &p, const mat_t<PT> &dW,
                                const PT &target_e) {

  // Init variables
  size_t i = 0;
  vec_t<PT> p_calibrated = p;
  PT residual;
  vec_t<PT> grad;

  std::cout << "Parameter calibration ... \n";

  do {
    // Calculate passive result, gradient and Hessian
    auto [e, dedp, dedpp] =
        euler_maruyama_combined_driver(np, ns, x0, p_calibrated, dW);

    // PT e = euler_maruyama(np, ns, x0, p_calibrated, dW);
    PT residual = e - target_e;

    // Calculate gradient of the parameters w.r.t. the residual
    // vec_t<PT> dedp = euler_maruyama_gradient(np, ns, x0, p_calibrated, dW);
    grad = 2 * dedp * residual;

    // Calculate Hessian of the parameters w.r.t. the residual
    // mat_t<PT> dedpp = euler_maruyama_hessian(np, ns, x0, p_calibrated, dW);
    mat_t<PT> H = 2 * dedpp * residual + 2 * dedp * dedp.transpose();

    // Perform Newton step
    p_calibrated = p_calibrated - H.lu().solve(grad);

    std::cout << "Step " << i++ << ": Residual = " << residual
              << ", Gradient norm = " << grad.norm() << "\n";

    // Check for divergence
    if (grad.norm() > 1e30) {
      std::cout << "Error: Newton algorithm diverged!\n";
      break;
    }
  } while (grad.norm() > 1e-6);

  std::cout << "... done.\n\n";
  return p_calibrated;
}

// ************************************************************************** //

// Main entry point
int main(const int argc, const char *argv[]) {
  using PT = double;

  assert(argc <= 5);
  auto start = std::chrono::high_resolution_clock::now();

  // Number of paths we calculate to approximate the expected value
  size_t np = 2000;
  if (argc > 1) {
    np = static_cast<size_t>(std::stoull(argv[1]));
  }

  // Number of steps in the Euler-Maruyama scheme
  size_t ns = 4;
  if (argc > 2) {
    ns = static_cast<size_t>(std::stoull(argv[2]));
  }

  // Time step
  PT dt = T / ns;

  // Starting value and parameter vector
  PT x0 = 1.0;
  vec_t<PT> p = vec_t<PT>::Constant(ns, 1.0);
  PT target_e = 2.5;

  // Generate np sets of ns randomly distributed variables
  mat_t<PT> dW = std::sqrt(dt) *
                 (mat_t<PT>::Random(np, ns) + mat_t<PT>::Constant(np, ns, 1.0));

  // Combined driver
  auto [e_2, dedp_2, ddedpp_2] =
      euler_maruyama_combined_driver(np, ns, x0, p, dW);

  // Passive result
  PT e = euler_maruyama(np, ns, x0, p, dW);

  // Handwritten results
  vec_t<PT> dedp = hwt1::euler_maruyama_gradient(np, ns, x0, p, dW);
  mat_t<PT> ddedpp = hwt2::euler_maruyama_hessian(np, ns, x0, p, dW);

  std::cout << "Passive error: " << e - e_2 << "\n";
  std::cout << "Gradient error: " << (dedp - dedp_2).norm() << "\n";
  std::cout << "Hessian error: " << (ddedpp - ddedpp_2).norm() << "\n\n";

  // // // Perform parameter calibration
  // vec_t<PT> p_calibrated = parameter_calibration(np, ns, x0, p, dW,
  // target_e); e = euler_maruyama(np, ns, x0, p_calibrated, dW); std::cout <<
  // "Target for expected value for x: " << target_e << "\n"; std::cout <<
  // "Target error: " << e - target_e << "\n"; std::cout << "Calibrated
  // parameters:\n" << p_calibrated << std::endl;

  return 0;
}

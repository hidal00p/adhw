# Exploitation of sparsity

## 1. Sparse J
Initial J:
```text
a b 0 0 0 0
c 0 0 d 0 0
e 0 f g 0 0
0 h 0 0 i j
0 0 k 0 l m
0 0 0 n o p
```

Reordered J:
```text
a b 0 0 0 0
c 0 0 d 0 0
e 0 f g 0 0
0 h 0 0 i j
0 0 k 0 l m
0 0 0 n o p
```

## 2. Bandwidth
It does not seem that the bandwidth can be reduced by reordering.

* Initial J bandwidth -- 2
* Reordered J bandwidth -- 2

The initial J will be used hereafter.

## 3. Tangent mode
**NOTE**: Since there are rows with 3 occupied elements, the matrix can be compressed
to maximum a 6x3 dimension. So we expect to either have that or higher dimensionality.

Compressed J:
```text
a b 0 0
c 0 d 0
e f g 0
j h 0 i
m k 0 l
p 0 n o
```

Seed matrix:
```text
a b 0 0
c 0 d 0
e f g 0
j h 0 i
m k 0 l
p 0 n o
```

Full harvesting equation:
```text
|a b 0 0 0 0| |1 0 0 0|   |a b 0 0|
|c 0 0 d 0 0| |0 1 0 0|   |c 0 d 0|
|e 0 f g 0 0| |0 1 0 0| = |e f g 0| 
|0 h 0 0 i j| |0 0 1 0|   |j h 0 i|
|0 0 k 0 l m| |0 0 0 1|   |m k 0 l|
|0 0 0 n o p| |1 0 0 0|   |p 0 n o|
```

## 4. Adjoint mode
**NOTE**: Since there are columns with 3 occupied elements, the matrix can be compressed
to maximum a 3x6 dimension. So we expect to either have that or higher dimensionality.

Compressed J:
```text
a b 0 n o p
c 0 k d l m
e h f g i j
```

Seed matrix:
```text
1 0 0 0 0 1
0 1 0 0 1 0
0 0 1 1 0 0
```

Full harvesting equation:
```text
              |a b 0 0 0 0|   
|1 0 0 0 0 1| |c 0 0 d 0 0|   |a b 0 n o p|
|0 1 0 0 1 0| |e 0 f g 0 0| = |c 0 k d l m|
|0 0 1 1 0 0| |0 h 0 0 i j|   |e h f g i j|
              |0 0 k 0 l m|   
              |0 0 0 n o p|   
```

// ************************************************************************** //
//       Introduction to Algorithmic Differentiation SS23 - Tutorial 9        //
//                                                                            //
//                          Handwritten Adjoint Code                          //
//                                                                            //
// Copyright (C) 2023 Software and Tools for Computational Engineering (STCE) //
//               RWTH Aachen University - www.stce.rwth-aachen.de             //
// ************************************************************************** //

// Include Eigen
#include <Eigen/Dense>

// Include C++ standard header
#include <algorithm>
#include <array>
#include <cassert>
#include <chrono>
#include <cmath>
#include <iostream>

using namespace std;

// *********************** Euler-Maruyama Parameters ************************ //

// Time interval we want to solve: [0, T]
constexpr double T = 1.0;

// ****************************** Custom Types ****************************** //

//! Vector (inputs & Gradient)
template <typename T> using vec_t = Eigen::VectorX<T>;

//! Matrix (Hessian)
template <typename T> using mat_t = Eigen::MatrixX<T>;

// ******************************** Primals ********************************* //

// Function f in the SDE
template <typename AT, typename PT>
AT f(const size_t i, const AT &x, const vec_t<AT> &p, const PT &t) {
  return p(i) * sin(x * t);
}

// Function g in the SDE
template <typename AT, typename PT>
AT g(const size_t i, const AT &x, const vec_t<AT> &p, const PT &t) {
  return p(i) * cos(x * t);
}

// Calculate a single path
template <typename AT, typename PT>
AT single_path(const int j, const size_t ns, const PT &x0, const vec_t<AT> &p,
               const mat_t<PT> &dW) {
  // Init state variables
  AT x = x0;
  PT t = 0;

  // Time step
  PT dt = T / ns;

  // Perform steps
  for (size_t i = 0; i < ns; i++) {
    x += f(i, x, p, t) * dt + g(i, x, p, t) * dW(j, i);
    t += dt;
  }

  return x;
}

// Calculate expected value (average) over all paths
template <typename AT, typename PT>
AT euler_maruyama(const size_t np, const size_t ns, const PT &x0,
                  const vec_t<AT> &p, const mat_t<PT> &dW) {
  AT expected_value = 0;
  for (size_t j = 0; j < np; j++) {
    expected_value += single_path(j, ns, x0, p, dW);
  }
  expected_value /= np;

  return expected_value;
}

// ****************** Hand-written adjoints (first order) ******************* //

namespace hwa1 {

template <typename AT> struct adjoint_res {
  AT primal;
  vec_t<AT> grad;
};

template <typename AT, typename PT>
void f(adjoint_res<AT> &F_a, const AT &x, const AT &p, const PT &t) {

  AT tmp = sin(x * t);
  F_a.primal = p * tmp;

  F_a.grad[0] = p * t * cos(x * t);
  F_a.grad[1] = tmp;
}

// Function g in the SDE
template <typename AT, typename PT>
void g(adjoint_res<AT> &G_a, const AT &x, const AT &p, const PT &t) {

  AT tmp = cos(x * t);
  G_a.primal = p * tmp;

  G_a.grad[0] = -p * t * sin(x * t);
  G_a.grad[1] = tmp;
}

// Calculate a single path
template <typename AT, typename PT>
void single_path(const int j, const size_t ns, const PT &x0, const vec_t<AT> &p,
                 vec_t<AT> &J,    // vector for the Jacobian
                 const AT y_seed, // dcdy - downstream context or adjoint seed
                 AT &x,           // result of the primal computation
                 const mat_t<PT> &dW) {
  x = x0;
  PT t = 0;

  // Time step
  PT dt = T / ns;

  PT dw = 0.0;

  // Storage stack for data flow reversal in adjoint computation
  vec_t<AT> p_A = vec_t<AT>::Zero(ns);
  vec_t<AT> x_A = vec_t<AT>::Zero(ns);

  // Stores derivatives wrt p and x
  adjoint_res<AT> F_a = {0.0, vec_t<AT>::Zero(2)};
  adjoint_res<AT> G_a = {0.0, vec_t<AT>::Zero(2)};

  // Perform steps
  for (size_t i = 0; i < ns; i++) {
    f(F_a, x, p[i], t);
    g(G_a, x, p[i], t);

    dw = dW(j, i);
    x += F_a.primal * dt + G_a.primal * dw;
    t += dt;

    x_A[i] = 1 + F_a.grad[0] * dt + G_a.grad[0] * dw;
    p_A[i] = F_a.grad[1] * dt + G_a.grad[1] * dw;
  }

  // Data flow reversal.
  // Computes all the derivatives wrt to p.
  AT acc = y_seed;
  for (size_t i = 0; i < ns; i++) {
    size_t idx = ns - 1 - i;
    J[idx] += acc * p_A[idx];
    acc *= x_A[idx];
  }
}

// Calculate:
// - expected value (average) over all paths.
// - Jacobian for all active variables.
template <typename AT, typename PT>
adjoint_res<AT> euler_maruyama(const size_t np, const size_t ns, const PT &x0,
                               const vec_t<AT> &p, const mat_t<PT> &dW) {

  AT expected_value = 0.0; // Primal result
  AT res = 0.0;            // For local computation

  vec_t<AT> J = vec_t<AT>::Zero(ns); // Jacobian vector

  for (size_t j = 0; j < np; j++) {
    single_path(j, ns, x0, p, J, 1.0, res, dW);
    expected_value += res;
  }

  expected_value /= np;
  J /= np;

  return {expected_value, J};
}
} // namespace hwa1

// ****************** Hand-written tangents (first order) ******************* //

// Tangent of f in the SDE
template <typename AT, typename PT>
void f_t1(const size_t i, const AT &x, const AT &x_t1, const vec_t<AT> &p,
          const vec_t<AT> &p_t1, const PT &t, AT &ret, AT &ret_t1) {

  ret_t1 = p_t1(i) * sin(x * t) + p(i) * x_t1 * t * cos(x * t);
  ret = p(i) * sin(x * t);
}

// Tangent of g in the SDE
template <typename AT, typename PT>
void g_t1(const size_t i, const AT &x, const AT &x_t1, const vec_t<AT> &p,
          const vec_t<AT> &p_t1, const PT &t, AT &ret, AT &ret_t1) {

  ret_t1 = p_t1(i) * cos(x * t) - p(i) * x_t1 * t * sin(x * t);
  ret = p(i) * cos(x * t);
}

// Tangent of a single path
template <typename AT, typename PT>
void single_path_t1(const int j, const size_t ns, const PT &x0,
                    const vec_t<AT> &p, const vec_t<AT> &p_t1,
                    const mat_t<PT> &dW, AT &ret, AT &ret_t1) {

  // Init state variables
  AT x_t1 = 0;
  AT x = x0;
  PT t = 0;

  // Time step
  PT dt = T / ns;

  // Perform steps
  for (size_t i = 0; i < ns; i++) {
    AT ret_f_t1 = 0;
    AT ret_f = 0;
    f_t1(i, x, x_t1, p, p_t1, t, ret_f, ret_f_t1);

    AT ret_g_t1 = 0;
    AT ret_g = 0;
    g_t1(i, x, x_t1, p, p_t1, t, ret_g, ret_g_t1);

    x_t1 += ret_f_t1 * dt + ret_g_t1 * dW(j, i);
    x += ret_f * dt + ret_g * dW(j, i);
    t += dt;
  }

  ret_t1 = x_t1;
  ret = x;
}

// Tangent of the expected value (average) over all paths
template <typename AT, typename PT>
void euler_maruyama_t1(const size_t np, const size_t ns, const PT &x0,
                       const vec_t<AT> &p, const vec_t<AT> &p_t1,
                       const mat_t<PT> &dW, AT &ret, AT &ret_t1) {

  AT expected_value_t1 = 0;
  AT expected_value = 0;

  for (size_t j = 0; j < np; j++) {
    AT ret_single_path_t1 = 0;
    AT ret_single_path = 0;
    single_path_t1(j, ns, x0, p, p_t1, dW, ret_single_path, ret_single_path_t1);

    expected_value_t1 += ret_single_path_t1;
    expected_value += ret_single_path;
  }

  expected_value_t1 /= np;
  expected_value /= np;

  ret_t1 = expected_value_t1;
  ret = expected_value;
}

// ****************** Hand-written tangents (second order) ****************** //

// Tangent over tangent of f in the SDE
template <typename AT, typename PT>
void f_t1_t2(const size_t i, const AT &x, const AT &x_t2, const AT &x_t1,
             const AT &x_t1_t2, const vec_t<AT> &p, const vec_t<AT> &p_t2,
             const vec_t<AT> &p_t1, const vec_t<AT> &p_t1_t2, const PT &t,
             AT &ret, AT &ret_t2, AT &ret_t1, AT &ret_t1_t2) {

  ret_t1_t2 = p_t1_t2(i) * sin(x * t) + p_t1(i) * x_t2 * t * cos(x * t) +
              p_t2(i) * x_t1 * t * cos(x * t) +
              p(i) * x_t1_t2 * t * cos(x * t) -
              p(i) * x_t1 * t * x_t2 * t * sin(x * t);
  ret_t1 = p_t1(i) * sin(x * t) + p(i) * x_t1 * t * cos(x * t);
  ret_t2 = p_t2(i) * sin(x * t) + p(i) * x_t2 * t * cos(x * t);
  ret = p(i) * sin(x * t);
}

// Tangent over tangent of g in the SDE
template <typename AT, typename PT>
void g_t1_t2(const size_t i, const AT &x, const AT &x_t2, const AT &x_t1,
             const AT &x_t1_t2, const vec_t<AT> &p, const vec_t<AT> &p_t2,
             const vec_t<AT> &p_t1, const vec_t<AT> &p_t1_t2, const PT &t,
             AT &ret, AT &ret_t2, AT &ret_t1, AT &ret_t1_t2) {

  ret_t1_t2 = p_t1_t2(i) * cos(x * t) - p_t1(i) * x_t2 * t * sin(x * t) -
              p_t2(i) * x_t1 * t * sin(x * t) -
              p(i) * x_t1_t2 * t * sin(x * t) -
              p(i) * x_t1 * t * x_t2 * t * cos(x * t);
  ret_t1 = p_t1(i) * cos(x * t) - p(i) * x_t1 * t * sin(x * t);
  ret_t2 = p_t2(i) * cos(x * t) - p(i) * x_t2 * t * sin(x * t);
  ret = p(i) * cos(x * t);
}

// Tangent over tangent of a single path
template <typename AT, typename PT>
void single_path_t1_t2(const int j, const size_t ns, const PT &x0,
                       const vec_t<AT> &p, const vec_t<AT> &p_t2,
                       const vec_t<AT> &p_t1, const vec_t<AT> &p_t1_t2,
                       const mat_t<PT> &dW, AT &ret, AT &ret_t2, AT &ret_t1,
                       AT &ret_t1_t2) {

  // Init state variables
  AT x_t1_t2 = 0;
  AT x_t1 = 0;
  AT x_t2 = 0;
  AT x = x0;
  PT t = 0;

  // Time step
  PT dt = T / ns;

  // Perform steps
  for (size_t i = 0; i < ns; i++) {
    AT ret_f_t1_t2 = 0;
    AT ret_f_t1 = 0;
    AT ret_f_t2 = 0;
    AT ret_f = 0;
    f_t1_t2(i, x, x_t2, x_t1, x_t1_t2, p, p_t2, p_t1, p_t1_t2, t, ret_f,
            ret_f_t2, ret_f_t1, ret_f_t1_t2);

    AT ret_g_t1_t2 = 0;
    AT ret_g_t1 = 0;
    AT ret_g_t2 = 0;
    AT ret_g = 0;
    g_t1_t2(i, x, x_t2, x_t1, x_t1_t2, p, p_t2, p_t1, p_t1_t2, t, ret_g,
            ret_g_t2, ret_g_t1, ret_g_t1_t2);

    x_t1_t2 += ret_f_t1_t2 * dt + ret_g_t1_t2 * dW(j, i);
    x_t1 += ret_f_t1 * dt + ret_g_t1 * dW(j, i);
    x_t2 += ret_f_t2 * dt + ret_g_t2 * dW(j, i);
    x += ret_f * dt + ret_g * dW(j, i);
    t += dt;
  }

  ret_t1_t2 = x_t1_t2;
  ret_t1 = x_t1;
  ret_t2 = x_t2;
  ret = x;
}

// Tangent over tangent of the expected value (average) over all paths
template <typename AT, typename PT>
void euler_maruyama_t1_t2(const size_t np, const size_t ns, const PT &x0,
                          const vec_t<AT> &p, const vec_t<AT> &p_t2,
                          const vec_t<AT> &p_t1, const vec_t<AT> &p_t1_t2,
                          const mat_t<PT> &dW, AT &ret, AT &ret_t2, AT &ret_t1,
                          AT &ret_t1_t2) {

  AT expected_value_t1_t2 = 0;
  AT expected_value_t1 = 0;
  AT expected_value_t2 = 0;
  AT expected_value = 0;

  for (size_t j = 0; j < np; j++) {
    AT ret_single_path_t1_t2 = 0;
    AT ret_single_path_t1 = 0;
    AT ret_single_path_t2 = 0;
    AT ret_single_path = 0;
    single_path_t1_t2(j, ns, x0, p, p_t2, p_t1, p_t1_t2, dW, ret_single_path,
                      ret_single_path_t2, ret_single_path_t1,
                      ret_single_path_t1_t2);

    expected_value_t1_t2 += ret_single_path_t1_t2;
    expected_value_t1 += ret_single_path_t1;
    expected_value_t2 += ret_single_path_t2;
    expected_value += ret_single_path;
  }

  expected_value_t1_t2 /= np;
  expected_value_t1 /= np;
  expected_value_t2 /= np;
  expected_value /= np;

  ret_t1_t2 = expected_value_t1_t2;
  ret_t1 = expected_value_t1;
  ret_t2 = expected_value_t2;
  ret = expected_value;
}

// ************************* Drivers ************************** //

// Euler-Maruyama adjoint gradient driver
template <typename PT>
vec_t<PT> euler_maruyama_gradient_a(const size_t np, const size_t ns,
                                    const PT &x0, const vec_t<PT> &p,
                                    const mat_t<PT> &dW) {

  // All heavy work along with seeding and
  // harvesting through interpretation occurs
  // inside the hwa1::euler_maruyama call
  hwa1::adjoint_res<PT> res_A = hwa1::euler_maruyama(np, ns, x0, p, dW);
  return res_A.grad;
}

// Euler-Maruyama tangent gradient driver
template <typename PT>
vec_t<PT> euler_maruyama_gradient_t(const size_t np, const size_t ns,
                                    const PT &x0, const vec_t<PT> &p,
                                    const mat_t<PT> &dW) {

  // Init Gradient
  vec_t<PT> dedp = vec_t<PT>::Zero(p.size());

  // Tangent parameters
  vec_t<PT> p_t1 = vec_t<PT>::Zero(p.size());

  // Harvest derivatives
  for (size_t i = 0; i < ns; i++) {
    p_t1(i) = 1.0; // seed tangent p^{(1)}

    PT ret_t1 = 0;
    PT ret = 0;
    euler_maruyama_t1(np, ns, x0, p, p_t1, dW, ret, ret_t1);

    dedp(i) = ret_t1; // harvest tangent e^{(1)}
    p_t1(i) = 0.0;    // reset tangent p^{(1)}
  }

  return dedp;
}

// Euler-Maruyama tangent over tangent hessian driver
template <typename PT>
mat_t<PT> euler_maruyama_hessian_tot(const size_t np, const size_t ns,
                                     const PT &x0, const vec_t<PT> &p,
                                     const mat_t<PT> &dW) {

  // Init Hessian
  mat_t<PT> dedpp = mat_t<PT>::Zero(p.size(), p.size());

  // Tangent parameters
  vec_t<PT> p_t1_t2 = vec_t<PT>::Zero(p.size());
  vec_t<PT> p_t1 = vec_t<PT>::Zero(p.size());
  vec_t<PT> p_t2 = vec_t<PT>::Zero(p.size());

  for (size_t i = 0; i < ns; i++) {
    p_t1(i) = 1.0; // seed tangent p^{(1)}

    for (size_t j = 0; j <= i; j++) {
      p_t2(j) = 1.0; // seed tangent p^{(2)}

      PT ret_t1_t2 = 0;
      PT ret_t1 = 0;
      PT ret_t2 = 0;
      PT ret = 0;
      euler_maruyama_t1_t2(np, ns, x0, p, p_t2, p_t1, p_t1_t2, dW, ret, ret_t2,
                           ret_t1, ret_t1_t2);

      dedpp(j, i) = ret_t1_t2; // harvest tangent p^{(1,2)}
      p_t2(j) = 0.0;           // reset tangent p^{(2)}

      // Use the fact that Hessian is symmetric
      dedpp(i, j) = dedpp(j, i);
    }
    p_t1(i) = 0.0; // reset tangent p^{(1)}
  }

  return dedpp;
}

// ************************************************************************** //

// Calibration driver using Newton's algorithm
template <typename PT>
vec_t<PT> parameter_calibration(const size_t np, const size_t ns, const PT &x0,
                                const vec_t<PT> &p, const mat_t<PT> &dW,
                                const PT &target_e) {

  // Init variables
  size_t i = 0;
  vec_t<PT> p_calibrated = p;
  PT residual;
  vec_t<PT> grad;

  std::cout << "Parameter calibration ... \n";

  do {
    // Calculate residual
    PT e = euler_maruyama(np, ns, x0, p_calibrated, dW);
    PT residual = e - target_e;

    // Calculate gradient of the parameters w.r.t. the residual
    vec_t<PT> dedp = euler_maruyama_gradient_t(np, ns, x0, p_calibrated, dW);
    grad = 2 * dedp * residual;

    // Calculate Hessian of the parameters w.r.t. the residual
    mat_t<PT> dedpp = euler_maruyama_hessian_tot(np, ns, x0, p_calibrated, dW);
    mat_t<PT> H = 2 * dedpp * residual + 2 * dedp * dedp.transpose();

    // Perform Newton step
    p_calibrated = p_calibrated - H.lu().solve(grad);

    std::cout << "Step " << i++ << ": Residual = " << residual
              << ", Gradient norm = " << grad.norm() << "\n";

    // Check for divergence
    if (grad.norm() > 1e30) {
      std::cout << "Error: Newton algorithm diverged!\n";
      break;
    }
  } while (grad.norm() > 1e-6);

  std::cout << "... done.\n\n";
  return p_calibrated;
}

// ************************************************************************** //

// Main entry point
int main(const int argc, const char *argv[]) {
  using PT = double;

  assert(argc <= 5);
  auto start = std::chrono::high_resolution_clock::now();

  // Number of paths we calculate to approximate the expected value
  size_t np = 2000;
  if (argc > 1) {
    np = static_cast<size_t>(std::stoull(argv[1]));
  }

  // Number of steps in the Euler-Maruyama scheme
  size_t ns = 10;
  if (argc > 2) {
    ns = static_cast<size_t>(std::stoull(argv[2]));
  }

  // Time step
  PT dt = T / ns;

  // Starting value and parameter vector
  PT x0 = 1.0;
  vec_t<PT> p = vec_t<PT>::Constant(ns, 1.0);
  PT target_e = 2.5;

  // Generate np sets of ns randomly distributed variables
  mat_t<PT> dW = std::sqrt(dt) *
                 (mat_t<PT>::Random(np, ns) + mat_t<PT>::Constant(np, ns, 1.0));
  // std::random_device rd;
  // std::mt19937 gen(rd());
  // std::normal_distribution<double> normal(0, std::sqrt(dt));
  // mat_t<PT> dW = mat_t<PT>::NullaryExpr([&](){
  //   return normal(gen);
  // });
  // std::cout << "Random values:\n" << dW << "\n\n";

  // Passive result
  PT e = euler_maruyama(np, ns, x0, p, dW);
  std::cout << "Expected value for x: " << e << "\n";

  // Gradient computation
  vec_t<PT> dedp_t = euler_maruyama_gradient_t(np, ns, x0, p, dW);
  vec_t<PT> dedp_a = euler_maruyama_gradient_a(np, ns, x0, p, dW);

  std::cout << "Gradient error: " << (dedp_t - dedp_a).norm() << "\n";
  std::cout << "Gradient in tangent mode: \n" << dedp_t << "\n\n";
  std::cout << "Gradient in adjoint mode: \n" << dedp_a << "\n\n";

  // Hessian dedpp
  mat_t<PT> dedpp = euler_maruyama_hessian_tot(np, ns, x0, p, dW);
  std::cout << "Hessian dedpp: \n" << dedpp << "\n\n";

  // Perform parameter calibration
  vec_t<PT> p_calibrated = parameter_calibration(np, ns, x0, p, dW, target_e);
  e = euler_maruyama(np, ns, x0, p_calibrated, dW);
  std::cout << "Target for expected value for x: " << target_e << "\n";
  std::cout << "Target error: " << e - target_e << "\n";
  std::cout << "Calibrated parameters:\n" << p_calibrated << std::endl;

  return 0;
}

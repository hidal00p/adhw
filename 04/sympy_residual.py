#! /usr/bin/env python3

from sympy import init_printing, IndexedBase, ccode, diff
from sympy import Matrix, pprint, sin

init_printing()

# Init symbolic vector and calculate Rosenbrock function
x = IndexedBase("x", shape=(2,))
r = Matrix([x[0] ** 2 + x[1] ** 2 - 1, sin(x[0]) - x[1]])

print("r:")
pprint(r)
print()
print("Primal code:")
print("r(0) = " + ccode(r[0]) + ";")
print("r(1) = " + ccode(r[1]) + ";")
print()

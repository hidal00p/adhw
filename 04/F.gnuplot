#!/usr/bin/env gnuplot

set xyplane relative 0
set ztics norangelimit logscale autofreq
set title ""
set xlabel "x0"
set xrange [-2:2]
set ylabel "x1"
set yrange [-2:2]
set size square
set isosamples 500, 500

set parametric
set trange [-2*pi:2*pi]
plot cos(t),sin(t) title "x0^2 + x1^2 = 1"
replot t,sin(t) title "sin(x0) - x1 = 0"

pause mouse close

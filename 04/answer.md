### (8) Non-convergent initial guess
Newton method relies on updating the solution estimation via
searching for, where the tangent hyperplane crosses 0-plane. 

If the hyperplane is parallel to the 0-plane, then there is no point
of intersection, and hence the agorithm should diverge pretty much
immediately.

For our system of equations both sin(x0), as well as F(x0, x1) = x0^2 + x1^2
have an extremum at (x0, x1) = (0.0, 0.0), and since this is the point of
divergence.


### (9) SAC and DAG

For the implementation see [nls.cpp](nls.cpp)::F\_SAC.

DAG:
```text
          [v0=x0]     [v1=x1]
              |           |_____________________
             / ----------/------------         |
           2v0          2v1       cos(v0)      -1
           /           /             |         |
        [v2=(^2)]   [v3=(^2)]   [v5=(sin)]  [v6=(*-1)]
            \         /             |          |
             1       1              1          1
              \     /                \        /
             [v4=(+)-1]               [v7=(+)]
```

Alternatively see [dag.png](./dag/dag.png)

// ************************************************************************** //
//       Introduction to Algorithmic Differentiation SS23 - Tutorial 4        //
//                                                                            //
//                     First Order Tangents & Vector Mode                     //
//                                                                            //
// Copyright (C) 2023 Software and Tools for Computational Engineering (STCE) //
//               RWTH Aachen University - www.stce.rwth-aachen.de             //
// ************************************************************************** //

// Include Eigen & dco
#include <Eigen/Dense>
#include <dco.hpp>

// Include C++ standard header
#include <algorithm>
#include <array>
#include <cassert>
#include <cmath>
#include <iostream>

// ****************************** Custom types ****************************** //

//! Vector with 2 entries (inputs & Gradient)
template <typename T> using vec2_t = Eigen::Vector2<T>;

//! Matrix with 2x2 entries (Hessian)
template <typename T> using mat2_t = Eigen::Matrix2<T>;

// ***************************** Primal function **************************** //

/******************************************************************************
 * @brief Evaluate the residual of the system of non-linear equations.
 *
 * @param[in] x Input vector x.
 * @returns The residual r of the system of non-linear equations.
 ******************************************************************************/
template <typename T> vec2_t<T> F(const vec2_t<T> &x) {
  T x0 = x(0);
  T x1 = x(1);

  T y0 = x0 * x0 + x1 * x1 - 1;
  T y1 = sin(x0) - x1;

  return vec2_t<T>(y0, y1);
}

template <typename T> vec2_t<T> F_SAC(const vec2_t<T> &x) {
  // Inputs to the DAG or DAG root nodes
  T v0 = x(0);
  T v1 = x(1);

  // y0 path
  T v2 = v0 * v0;
  T v3 = v1 * v1;

  T v4 = v3 + v2 - 1;

  // y1 path
  T v5 = sin(v0);
  T v6 = -1 * v1;
  T v7 = v5 + v6;

  return vec2_t<T>(v4, v7);
}

// ************************* Analytical Derivatives ************************* //

/******************************************************************************
 * @brief Calculate the analytical Jacobian of the residual r.
 *
 * @param[in] x Input vector x.
 * @returns The analytical Jacobian of the residual r.
 ******************************************************************************/
template <typename T> mat2_t<T> dFdx(const vec2_t<T> &x) {
  T x0 = x(0);
  T x1 = x(1);

  T dy0dx0 = 2 * x0;
  T dy0dx1 = 2 * x1;

  T dy1dx0 = cos(x0);
  T dy1dx1 = -1;

  mat2_t<T> J{{dy0dx0, dy0dx1}, {dy1dx0, dy1dx1}};

  return J;
}

// ******************************** Tangents ******************************** //

/******************************************************************************
 * @brief Calculate the tangent Jacobian of the residual r.
 *
 * @param[in] x Input value x.
 * @returns The Jacobian of the residual r.
 ******************************************************************************/
template <typename T> mat2_t<T> dFdx_tangent(const vec2_t<T> &x) {
  using DCO_S = typename dco::gt1s<T>::type;

  vec2_t<DCO_S> X;
  vec2_t<DCO_S> Y;
  mat2_t<T> J;

  // initialization of the vector of overloaded types
  for (int i = 0; i < x.size(); i++)
    dco::value(X(i)) = x(i);

  for (int i = 0; i < x.size(); i++) {
    dco::derivative(X(i)) = 1.0; // seed

    Y = F(X);

    // harvest
    J(0, i) = dco::derivative(Y(0));
    J(1, i) = dco::derivative(Y(1));

    dco::derivative(X(i)) = 0.0; // clear
  }

  return J;
}

/******************************************************************************
 * @brief Calculate the tangent Jacobian of the residual r using vector mode.
 *
 * @param[in] x Input value x.
 * @returns The Jacobian of the residual r.
 ******************************************************************************/
template <typename T> mat2_t<T> dFdx_tangent_vector(const vec2_t<T> &x) {
  const int vec_size = 2;
  assert(vec_size >= x.size());
  using DCO_V = typename dco::gt1v<T, vec_size>::type;

  vec2_t<DCO_V> X;
  mat2_t<T> J;

  // seed
  for (int i = 0; i < x.size(); i++) {
    dco::value(X(i)) = x(i);
    dco::derivative(X(i))[i] = 1.0;
  }

  vec2_t<DCO_V> Y = F(X);

  // harvest
  for (int i = 0; i < Y.size(); i++) {
    for (int j = 0; j < X.size(); j++) {
      J(i, j) = dco::derivative(Y[i])[j];
    }
  }

  return J;
}

// ****************************** Optimization ****************************** //

/******************************************************************************
 * @brief Newton's algorithm to find the root if the residual r, i.e. the
 *        intersection of the implicitly defined functions.
 *
 * @param[in] x0 Initial guess.
 * @param[in] accuracy Accuracy threshold. Algorithm stops if the norm of
 *                     the residual falls below this accuracy.
 * @param[in] verbose Whether to print verbose output or not.
 * @returns The root of the residual function, if successful.
 ******************************************************************************/
template <typename T>
vec2_t<T> newton(const vec2_t<T> &x0, const T accuracy,
                 const bool verbose = true) {

  // Init variables
  size_t k = 0;
  vec2_t<T> x = x0;
  vec2_t<T> y;

  auto print_step = [&]() {
    if (verbose) {
      std::cout << k++ << " " << x.transpose() << " " << y.transpose() << " "
                << y.norm() << "\n";
    }
  };

  do {
    y = F(x);
    mat2_t<T> J = dFdx_tangent_vector(x);

    /*
     * On the line below
     * Y = F'(x_i)*(x - x_i) + F(x_i), i.e. Y = 0 for rooting searching
     * F'(x_i)*(x_i - x) = F(x_i), where F' := J, and F := y ->
     * J*(x_i - x) = y -> J.lu().solve(y) = x_total
     * x_i - x = x_total -> x = x_i - x_total -> x_i -= x_total =
     * J.lu().solve(y)
     */

    // Perform Newton step
    x -= J.lu().solve(y);

    print_step();

    // Check for divergence
    if (y.norm() > 1e30) {
      if (verbose) {
        std::cout << "Error: Newton algorithm diverged!" << std::endl;
      }
      break;
    }
  } while (y.norm() > accuracy);

  return x;
}

// ********************************** Main ********************************** //

int main(const int argc, const char *argv[]) {
  using T = double;

  assert(argc <= 3);

  // Accuracy
  T accuracy = 1e-6;
  if (argc > 1) {
    accuracy = static_cast<T>(std::stod(argv[1]));
  }

  // Verbosity
  bool verbose = false;
  if (argc > 2) {
    verbose = static_cast<bool>(std::stoi(argv[2]));
  }

  vec2_t<T> x0 = {0.005, 0.002};
  std::cout << std::endl << F(x0) << std::endl;
  std::cout << std::endl << dFdx(x0) << std::endl;
  std::cout << std::endl << dFdx_tangent(x0) << std::endl;
  std::cout << std::endl << dFdx_tangent_vector(x0) << std::endl;

  // solution of the system
  std::vector<vec2_t<T>> x = {{-0.005, -0.002}, {0.005, 0.002}};
  for (vec2_t<T> x_init : x) {
    vec2_t<T> x_root = newton(x_init, accuracy, verbose);
    std::cout << "=====" << std::endl;
    std::cout << "Solution is:\n" << x_root << std::endl;
    std::cout << "=====" << std::endl;
  }

  // Divergence case: (x0, x1) = (0.0, 0.0)
  std::cout << "Divergence case" << std::endl;
  vec2_t<T> x_root = newton({0.0, 0.0}, accuracy, verbose);
  std::cout << x_root << std::endl;

  return 0;
}
